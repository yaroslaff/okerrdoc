# VERSION 
Проверяет версию клиента (не устарела ли она) на сервере. Устанавливает статус ERR если требуется обновление и OK если версия не требует срочного обновления. Статус может быть OK даже если установлена не последняя версия.

## Пример
Просто посмотреть версию:
~~~
$ okerrclient --version
2.0.71 (version)

$ okerrclient --dry -s VERSION SEQDUMP
Sequence name 'braconnier': #140459506231704 no parent
lastcmd: SEQDUMP
route:
details: 2.0.71 latest version
except: None
method: None
tags: []
variables: {}
data: (2): OK
~~~

Реальное использование можете посмотреть в конфигурации скриптов проекта, в серверном скрипте "version".


