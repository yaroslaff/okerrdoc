# GREP

## Аргументы

- **field**='' 
- **ignorecase**=''
- **inverse**='0'
- **re**=''

## Описание
Фильтрует список строк или структур по регулярному выражению (**re**). Если **inverse** равно '1', то результат обратный (будут оставлены те элементы, которые не подходят под шаблон). Если **ignorecase** равно '1', то регулярное выражение будет игнорировать регистр букв.

Если фильтруется список словарей, то фильтрация выполняется по полю, которое указывается в **field**.

## Примеры

Фильтрация строк



~~~
# has 'rr' substring
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" "GREP re=rr" JDUMP NONE
[
    "Starr",
    "Harrison"
]

# inverse. has NOT 'rr' 
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" "GREP re=rr inverse=1" JDUMP NONE
[
    "Lennon",
    "McCartney"
]

# case-insensitive
$ okerrclient -s "LIST Lennon McCartney STARR Harrison" 'GREP re=rr ignorecase=1' JDUMP NONE
[
    "STARR",
    "Harrison"
]

# very simple regex
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" 'GREP re=rr$' JDUMP NONE
[
    "Starr"
]
~~~

Фильтрация словарей:

Все логи, которые запакованы .gz:
~~~
$ sudo okerrclient -s "DIR path=/var/log" "GREP field=basename re=gz" 'FORMAT $basename' JDUMP NONE  | head
[
    "pm-suspend.log.3.gz",
    "alternatives.log.2.gz",
    "alternatives.log.5.gz",
    "dpkg.log.4.gz",
    "syslog.3.gz",
    "alternatives.log.6.gz",
    "syslog.5.gz",
    "alternatives.log.9.gz",
    "dpkg.log.6.gz",
~~~
