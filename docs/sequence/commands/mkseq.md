# MKSEQ 
Очень мощный метод, создает sequence скрипт на основе данных (data) и запускает ее. 

Если после MKSEQ указаны еще команды, они будут добавлены в конец каждого скрипта и исполнены за ним.   


## Входные данные

На входе MKSEQ тем или иным способом ожидает один или несколько sequence скриптов.

**Если data словарь (dictionary):**
Скрипт для MKSEQ поставляется в виде словаря, ключами являются номера команд (как в Бейсике!), а значениями - строки - сами команды с параметрами. Команды будут исполнены по порядку ключей. (команда с индексом 10 будет исполнена раньше чем команда с индексом 20)

Если на входе MKSEQ словарь, элементами которого являются не строки, а другие словари, то это обрабатывается как набор скриптов, каждый из которых будет исполнен отдельно.

**Если data список (list):**
Исполняются все команды из списка по порядку.

**Если data нет (None):**
MKSEQ берет код скрипта (скриптов) с сервера (по параметру path) и исполняет.


## Параметр (path)
Путь к скрипту в базе ключей проекта из одного или нескольких элементов, разделенных символом "|". 

Если path задан, то MKSEQ попробует взять скрипт из базы ключей, по первому путю, если не получится - то по второму, и так далее, до первой успешной попытки. Если ни одна попытка не удастся - будет выдано сообщение об ошибке, и команда возвратит None. Для успешного получения скрипта с сервера необходимо использовать правильные значения keyuser и keypass.
По умолчанию - пустая строка (в этом случае, скрипт будет браться из data контекста).   
   
## Примеры

Взять с сервера набор sequence-скриптов для конфигурации anyserver и исполнить их:
~~~
$ sudo okerrclient -s "MKSEQ conf:anyserver" 
okerr updated braconnier:uptime = 716118
okerr updated braconnier:la = 0.73
okerr updated braconnier:df-/ = 43.8
okerr updated braconnier:myip = 11.22.33.44
okerr updated braconnier:opentcp = 22(sshd) 25(master) 80(apache2) 139(sm..
okerr updated braconnier:version = 2.0.30 (nonzero)
okerr updated braconnier:df-inodes-/ = 5
okerr updated braconnier:maxlogsz = 198367
~~~

Взять с сервера и исполнить один скрипт (uptime):
~~~
$ sudo okerrclient -s "MKSEQ lib:uptime" 
okerr updated braconnier:uptime = 716177
~~~

Попробовать взять скрипт с ошибочного адреса:
~~~
$ okerrclient -s "MKSEQ conf:nosuchserver" 
get keys error: Failed all 1 keypath: conf:nosuchserver (textid: qweqwe)
~~~

Запрос с несколькими альтернативными путями:
~~~
$ sudo okerrclient -s "MKSEQ conf:nosuchserver|conf:anyserver" 
okerr updated braconnier:uptime = 716281
okerr updated braconnier:la = 0.23
okerr updated braconnier:df-/ = 43.8
okerr updated braconnier:myip = 11.22.33.44
okerr updated braconnier:opentcp = 22(sshd) 25(master) 80(apache2) 139(sm..
okerr updated braconnier:version = 2.0.30 (nonzero)
okerr updated braconnier:df-inodes-/ = 5
okerr updated braconnier:maxlogsz = 198367
~~~

Дополнение скриптов командой NONE, чтобы избежать отсылки результатов на сервер:
~~~
$ sudo okerrclient -s "MKSEQ conf:anyserver" NONE
~~~
Если добавить ключ -v: ```sudo okerrclient -v -s "MKSEQ conf:anyserver" NONE```, то можно будет увидеть, что данные берутся с сервера и исполняются. 


Исполнение всех скриптов из набора, и подробное исследование их результата через SEQDUMP:
~~~
$ sudo okerrclient -s "MKSEQ conf:anyserver" SEQDUMP NONE
Sequence name 'braconnier:uptime': #139766184451968 Parent 'braconnier': #139766184448944
lastcmd: SEQDUMP
route:
	NONE 
details: 8d 7h
method: numerical|diffmin=0
tags: [u'uptime']
variables: {
    "uptimesec": 716709
}
data: (6): 716709


Sequence name 'braconnier:la': #139766179837424 Parent 'braconnier': #139766184448944
lastcmd: SEQDUMP
route:
	NONE 
details: 0.21, 0.22, 0.22
method: numerical|maxlim=1
tags: [u'la']
variables: {
    "uptimesec": 716709
}
data: (4): 0.21

[...]
~~~


Посмотрим (командой **KEYVAL**) скрипт c сервера и сохраним его:
~~~
$ okerrclient -s "KEYVAL lib:uptime" JDUMP NONE
{
    "010": "NAME $_name:uptime",
    "020": "METHOD numerical diffmin=0",
    "030": "TAG uptime",
    "040": "UPTIME",
    "050": "SAVE uptimesec",
    "060": "DHMS",
    "070": "DETAILS",
    "080": "LOAD uptimesec"
}
$ okerrclient -s "KEYVAL lib:uptime" JDUMP NONE > /tmp/uptime.seq
~~~        

Загрузка файла в JSON структуру (используя **JFILE**):
~~~
$ okerrclient --tpconf FILE:read=/tmp/uptime.seq -s 'JFILE /tmp/uptime.seq' JDUMP NONE
{
    "010": "NAME $_name:uptime",
    "020": "METHOD numerical diffmin=0",
    "030": "TAG uptime",
    "040": "UPTIME",
    "050": "SAVE uptimesec",
    "060": "DHMS",
    "070": "DETAILS",
    "080": "LOAD uptimesec"
}
~~~

Исполнение скрипта (словарь в JSON) из файла:
~~~
$ okerrclient --tpconf FILE:read=/tmp/uptime.seq -s 'JFILE /tmp/uptime.seq' MKSEQ
okerr updated braconnier:uptime = 717387
~~~

Исполнение скрипта (простой текст) из файла:
~~~
$ cat /tmp/script.seq 
NAME asdf
STR OK

$ okerrclient  --tpconf FILE:read=/tmp/script.seq -s "FILE /tmp/script.seq" LIST MKSEQ JDUMP
"OK"
okerr updated asdf = OK
~~~

## Примечание
Обратите внимание, при сохранении скрипта в файл, okerrclient не пишет в файл напрямую, это делается через перенаправление вывода в шелле. При чтении файла через **JFILE**, необходимо предварительно явно указать это разрешение для Task Processor'а внутри клиента через параметр --tpconf (Task Processor Configuration).





