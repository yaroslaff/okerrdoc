# FROMJSON

Конвертирует строку в JSON формате в структуру данных.

## Примеры
~~~
$ okerrclient -s 'STR {"hw": "Hello world!", "food": [ "Beer", "Cognac", "Pizza", "Pelmeni" ] }' JDUMP NONE             # string
"{\"hw\": \"Hello world!\", \"food\": [ \"Beer\", \"Cognac\", \"Pizza\", \"Pelmeni\" ] }"
$ okerrclient -s 'STR {"hw": "Hello world!", "food": [ "Beer", "Cognac", "Pizza", "Pelmeni" ] }' FROMJSON JDUMP NONE    # data structure
{
    "food": [
        "Beer",
        "Cognac",
        "Pizza",
        "Pelmeni"
    ],
    "hw": "Hello world!"
}
~~~
