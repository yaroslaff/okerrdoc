# STR

Простая команда. Если задан аргумент, возвращает его. 'STR OK', 'STR ERR' полностью аналогичны командам **OK** и **ERR**.
Если аргумента нет, возвращает строковое представление данных (команда str(data) на языке python). Если входных данных нет,
возвращает пустую строку.

# Примеры
~~~
$ okerrclient --dry -s 'STR foo' DUMP 
foo

$ okerrclient --dry -s 'STR' JDUMP
""
~~~
