# STRIP

## Аргументы
**chars**='\r\n\t ' - символы для удаления


Удаляет специальные символы из начала и конца строки. Если на входе список строк, то выполняет эту процедуру с каждой строкой.

~~~
$ okerrclient -s "STR       bbbb     " JDUMP NONE
"      bbbb     "
$ okerrclient -s "STR       bbbb     " STRIP JDUMP NONE
"bbbb"

$ okerrclient -s 'LIST "aaa bbb " ccc' STRIP JDUMP NONE
[
    "aaa bbb",
    "ccc"
]
~~~
