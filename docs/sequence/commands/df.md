# DF

Информация о свободном месте на дисках.

## Аргументы
**path** - ''

## Описание
Если **path** не указан (''), выдает информацию о всех модмонтированных системах (кроме тех, что подмонтированы с устройств /dev/loop*) в виде списка словарей.

Если **path** указан, то выдает словарь только по одной партиции.

## Примеры
~~~
$ okerrclient -s DF JDUMP NONE
[
    {
        "free": 257692962816,
        "freeg": 239,
        "inodes_free": 28232070,
        "inodes_percent": 5,
        "inodes_total": 29974528,
        "path": "/",
        "percent": 43.8,
        "total": 483175751680,
        "totalg": 449,
        "used": 200915230720,
        "usedg": 187
    }
]
$ okerrclient -s 'DF path=/' JDUMP NONE
{
    "free": 257692966912,
    "freeg": 239,
    "inodes_free": 28232070,
    "inodes_percent": 5,
    "inodes_total": 29974528,
    "path": "/",
    "percent": 43.8,
    "total": 483175751680,
    "totalg": 449,
    "used": 200915226624,
    "usedg": 187
}
~~~

Как используется в okerr:
~~~
$ okerrclient -s "KEYVAL lib:df" JDUMP NONE
{
    "010": "NAME $_name:df",
    "020": "METHOD numerical maxlim=80",
    "030": "TAG df",
    "040": "DF",
    "050": "FILTER not '/media/' in path",
    "060": "FORK",
    "070": "EXPORT path=path",
    "080": "NAME $_name-$path",
    "090": "DETAILS $percent% ($usedg/${totalg}G used, $freeg left)",
    "100": "FORMAT $percent"
}
~~~
