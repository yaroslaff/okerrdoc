# NAME
Команда NAME устанавливает новое имя последовательности команд. Аргументом является новое имя.
Как часть имени может выступать текущее имя, через специальную переменная $_name, например:

## Примеры

~~~
$ okerrclient --name foo -s SEQDUMP NONE
Sequence name 'foo': #139965641860304 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {}
no data


$ okerrclient --name foo -s NAME SEQDUMP NONE
Sequence name 'foo:noname': #140022958073040 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {}
no data

$ okerrclient --name foo -s 'NAME $_name:bar'  SEQDUMP NONE
Sequence name 'foo:bar': #139843913737424 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {}
no data

$ okerrclient --name foo -s 'NAME bar'  SEQDUMP NONE
Sequence name 'bar': #139861315724496 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {}
no data

~~~
