# TAG

Устанавливает метку при создании индикатора. Если индикатор существует, команда эффекта не имеет.

## Параметр (tag)
Параметром задается имя метки. Если нужно установить несколько меток, команду можно повторять несколько раз.


## Пример:
~~~
$ okerrclient -s 'TAG test' 'TAG foo bar' SEQDUMP NONE
Sequence name 'braconnier': #139966312367312 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: ['test', 'foo bar']
variables: {}
no data
~~~        
