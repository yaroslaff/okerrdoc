# CONNECTIONS

Выдает список активных сетевых соединений

## Примеры

LAST используется для наглядности, чтобы получить всего 1 элемент.
~~~
$ sudo okerrclient -s CONNECTIONS LAST JDUMP NONE
{
    "basename": "ssh",
    "exe": "/usr/bin/ssh",
    "ip": "10.0.0.3",
    "name": "ssh",
    "pid": 30843,
    "port": 39708,
    "proto": "TCP",
    "status": "ESTABLISHED"
}
~~~

Пользователь root видит больше соединений:
~~~
$ okerrclient -s CONNECTIONS LEN JDUMP NONE
16
$ sudo okerrclient -s CONNECTIONS LEN JDUMP NONE
54
~~~

Только слушающие соединения (опять, LAST для наглядности):
~~~
$ sudo okerrclient -s CONNECTIONS 'FILTER status == "LISTEN"' LAST JDUMP NONE
{
    "basename": "cupsd",
    "exe": "/usr/sbin/cupsd",
    "ip": "127.0.0.1",
    "name": "cupsd",
    "pid": 6328,
    "port": 631,
    "proto": "TCP",
    "status": "LISTEN"
}
~~~
