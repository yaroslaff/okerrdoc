# LEN

Возвращает размер списка

## Примеры
~~~
# List of 3 elements
$ okerrclient -s 'LIST aaa bbb ccc' LEN DUMP NONE
3

# How much directory elements we have in /var/log?
$ sudo okerrclient -s 'DIR path=/var/log/' LEN DUMP NONE
570

~~~
