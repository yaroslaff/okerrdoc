# PROCESSES

Возвращает список процессов в системе

## Примеры
~~~
# First process in list.
$ okerrclient -s PROCESSES FIRST JDUMP NONE
{
    "basename": "init",
    "exe": "/sbin/init",
    "name": "init",
    "pid": 1,
    "username": "root"
}

# How many running processes do we have at moment?
$ okerrclient -s PROCESSES LEN JDUMP NONE
301

# Apache running or not?
$ sudo okerrclient -s PROCESSES 'FILTER name=="apache2"' LEN JDUMP NONE
6
$ sudo okerrclient -s PROCESSES 'FILTER name=="apache2"' LEN NONZERO JDUMP NONE
"OK"
$ sudo /etc/init.d/apache2 stop
 * Stopping web server apache2                                                                                                                  * 
$ sudo okerrclient -s PROCESSES 'FILTER name=="apache2"' LEN NONZERO JDUMP NONE
"ERR"
~~~
