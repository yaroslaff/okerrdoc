# FILTER
Фильтрует список словарей по выражению в стиле питон.

## Параметр (expr)
Логическое выражение. По умолчанию - True

## Описание
Для каждого словаря из списка, переменные иницилизируются в значения полей и затем безопасно исполняется выражение. Если выражение дало True, то словарь сохраняется в списке. Если False - то удаляется.


## Примеры
~~~
# How many directory entries in /var/log?
$ sudo okerrclient -s 'DIR path=/var/log' LEN JDUMP NONE
570

# Now, check only regular files
$ sudo okerrclient -s 'DIR path=/var/log' "FILTER type=='REG'" LEN JDUMP NONE
538

# Now, check only .gz regular files
$ sudo okerrclient -s 'DIR path=/var/log' "FILTER type=='REG' and '.gz' in basename" LEN JDUMP NONE
422

# Now, only .gz OR large files (we try to clean something to free disk space)
$ sudo okerrclient -s 'DIR path=/var/log' "FILTER type=='REG' and ('.gz' in basename or size<102400)" LEN JDUMP NONE
525

~~~
