# DETAILS

Устанавливает детали контекста (которые затем будут переданы на сервер и будут отображаться в поле "Детали" индикатора)

## Параметр (format)

Строка для установки в качестве деталей индикатора.

## Примеры
~~~
$ okerrclient -s 'DETAILS some technical details' SEQDUMP NONE
Sequence name 'braconnier': #140176691803488 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: some technical details
method: None
tags: []
variables: {}
no data
~~~


