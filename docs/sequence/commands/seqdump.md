# SEQDUMP

Команда служит для отладки, она печатает весь контекст, в том числе переменные, имя индикатора, детали, метод его обработки, метки, и частично печатает данные (data).

## Пример
~~~
$ okerrclient -s 'NAME test' 'METHOD numerical maxlim=100' 'SET aaa=bbb' 'STR 42' SEQDUMP NONE
Sequence name 'test': #140188836685080 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: numerical|maxlim=100
tags: []
variables: {
    "aaa": "bbb"
}
data: (2): 42
~~~
