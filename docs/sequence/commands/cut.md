# CUT

CUT	Convert list of dicts to list of strings


## Аргументы
* **sep**='' Значение сепаратора
* **start**='' Начальная позиция
* **stop**='' Конечная позиция

По аналогии с утилитой cut, служит для вырезания подстрок из строк по позициям или с учетом сепаратора.

Строка рассматривается как список букв, CUT вырезает подстроку между start и stop. Если start не указан - то с начала строки, если stop не указан - то до конца.
Если сепаратор указан, то строка разбивается (split) на поля, а затем обрезается аналогично, с учетом start и стоп.
Если на входе список - аналогичная операция выполняется для каждого элемента списка.

~~~
$ okerrclient -s 'STR aaa bbb ccc' "CUT start=0 stop=7" JDUMP NONE
"aaa bbb"

$ okerrclient -s 'STR aaa bbb ccc' "CUT sep=' '" JDUMP NONE
[
    "aaa",
    "bbb",
    "ccc"
]
$ okerrclient -s 'STR aaa bbb ccc' "CUT sep=' ' start=1" JDUMP NONE
[
    "bbb",
    "ccc"
]
$ okerrclient -s 'LIST Alpha Bravo Charlie Delta' "CUT start=0 stop=2" JDUMP NONE
[
    "Al",
    "Br",
    "Ch",
    "De"
]
~~~


Пример - взять последние строчки из лога, обрезать время:
~~~
$ tail -n 3 /var/log/syslog
Feb  7 01:02:18 braconnier wpa_supplicant[979]: wlan0: CTRL-EVENT-SCAN-STARTED 
Feb  7 01:04:09 braconnier wpa_supplicant[979]: wlan0: WPA: Group rekeying completed with 6c:3a:6a:41:b3:4d [GTK=CCMP]
Feb  7 01:04:18 braconnier wpa_supplicant[979]: wlan0: CTRL-EVENT-SCAN-STARTED 
$ okerrclient --tpconf FILE:read=/var/log/syslog -s 'FILE /var/log/syslog' LIST 'LAST 3' 'CUT start=16' JDUMP NONE
[
    "braconnier wpa_supplicant[979]: wlan0: CTRL-EVENT-SCAN-STARTED \n",
    "braconnier wpa_supplicant[979]: wlan0: WPA: Group rekeying completed with 6c:3a:6a:41:b3:4d [GTK=CCMP]\n",
    "braconnier wpa_supplicant[979]: wlan0: CTRL-EVENT-SCAN-STARTED \n"
]

~~~
