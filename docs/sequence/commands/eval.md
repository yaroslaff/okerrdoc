# EVAL
Выполняет питоновское выражение. Доступны переменные контекста и данные как переменная _data. Возвращает результат. Если результат булевый,
то True меняется на строку "OK", а False на "ERR".

# Примеры
~~~
$ okerrclient --dry -s "EVAL 3+5" DUMP
8
$ okerrclient --dry -s "EVAL 3>5" DUMP
ERR
$ okerrclient --dry -s "EVAL 3<5" DUMP
OK
$ sudo okerrclient --dry --tpconf RUN:safebin=/bin/true -s "RUN /bin/true" JDUMP "EVAL _data['code']" DUMP
{
    "code": 0,
    "stderr": "",
    "stdout": ""
}
0
$ sudo okerrclient --dry --tpconf RUN:safebin=/bin/true -s "RUN /bin/true" "EVAL _data['code'] == 0" DUMP
OK
$ sudo okerrclient --dry --tpconf RUN:safebin=/bin/false -s "RUN /bin/false" "EVAL _data['code'] == 0" DUMP
ERR
$ okerrclient --dry -s "SET aa=bb" 'EVAL "b" in aa' JDUMP
"OK"
$ okerrclient --dry -s "SET aa=bb" 'EVAL "z" in aa' JDUMP
"ERR"
$ okerrclient --dry -s "SET aa=bb" 'EVAL aa + "zzz"' JDUMP
"bbzzz"

$ okerrclient -s "SET aa=bb" 'EXCEPT Bad Code' 'EVAL len(aa)' JDUMP
Error in filter expression: Operaton type Call is not allowed
okerr updated braconnier = Bad Code
~~~


