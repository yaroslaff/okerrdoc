# SUM
Суммирует список чисел из строк или полей словарей.

# Аргумент (field)
Имя поля, которое суммируется.

## Примеры
~~~
$ okerrclient -s "LIST 1 2 3" SUM INT JDUMP NONE
6

# sum of regular file sizes of /tmp/dir/bbb
$ okerrclient -s 'DIR path=/tmp/dir/bbb/' 'FILTER type=="REG"' 'SUM size' JDUMP  NONE
8.0

~~~


