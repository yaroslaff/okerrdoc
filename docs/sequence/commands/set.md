# SET
Непосредственно устанавливает значение переменной.

## Параметр (set)
имя и значение переменной (varname=value)

## Пример
~~~
$ okerrclient -s 'SET aaa=bbb' SEQDUMP NONE
Sequence name 'braconnier': #139963570418968 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {
    "aaa": "bbb"
}
no data

# Single quotes around line with $variable, or shell will replace it
$ okerrclient -s "SET aaa=bbb" 'STR value of aaa is $aaa' DUMP NONE
value of aaa is bbb

~~~

