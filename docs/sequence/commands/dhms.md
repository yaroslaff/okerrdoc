# DHMS

Конвертирует количество секунд в удобочитаемый формат (дни, часы, минуты, секунды).

## Аргументы
	
* **destfield**=''
* **field**=''
* **num**='2' - количество элементов.

## Описание

Если на входе строка - конвертирует ее.
Если словарь или список словарей - конвертирует поле **field** каждого словаря. Если указано значение **destfield**, то результат записывается в него. Если не указано - то в **field**.

Так же см. **KMGT**.

## Примеры
~~~
$ okerrclient -s UPTIME DHMS DUMP NONE
10d 5h
$ okerrclient -s UPTIME "DHMS num=1" DUMP NONE
10d
$ okerrclient -s UPTIME "DHMS num=3" DUMP NONE
10d 5h 30m

# modification time age converted to human-readable form
$ okerrclient -s "DIR path=/etc/passwd" FIRST 'DHMS field=mage' JDUMP NONE
{
    "aage": 10284,
    "atime": 1486558099,
    "basename": "passwd",
    "cage": 14428794,
    "ctime": 1472139590,
    "depth": 0,
    "mage": "166d 23h",
    "mtime": 1472139590,
    "path": "/etc/passwd",
    "size": 2407,
    "type": "REG"
}

~~~

