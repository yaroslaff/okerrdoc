# EXPORT

Если данные - строка (), то экспортирует ее в переменную, указанную параметром. (аналогично команде SAVE)

Если данные - словарь и параметр - выражение вида varname=key, то в переменную varname загружается значение из поля key словаря.


## Параметр (exp)

Либо имя переменной (varname), либо выражение присваивания (varname=key). 

## Примеры

~~~
$ okerrclient -s 'STR this is string' 'EXPORT aaa' SEQDUMP NONE
Sequence name 'braconnier': #140461763527960 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {
    "aaa": "this is string"
}
data: (14): this is string

$ okerrclient -s 'DEFAULT {"name": "Sasha", "born": "14/03/1988"}' "EXPORT alarm=born" SEQDUMP NONE
Sequence name 'braconnier': #140571763275032 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {
    "alarm": "14/03/1988"
}
data: (43): {u'born': u'14/03/1988', u'name': u'Sasha'}
~~~

