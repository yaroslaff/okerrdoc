# FILE

Чтение файла

# Конфигурация
read (--tpconf FILE:read=path) - имя файла, который разрешено читать

# Параметр (path)
Имя файла для чтения

## Описание
Открывает файл для чтения, возвращает итерируемый указатель на файл. Далее его можно обработать командами, которые ожидают итератор на входе, такие как (MULTILINE или STRIP).

Файл должен быть разрешен для чтения через --tpconf.

## Примеры
~~~
$ okerrclient --tpconf FILE:read=/tmp/abc.txt -s 'FILE /tmp/abc.txt' STRIP DUMP NONE
['aaa', 'bbb', 'ccc', '']

# Not very secure if sequence script fetched over network
$ okerrclient --tpconf FILE:read=* -s 'FILE /tmp/abc.txt' STRIP DUMP NONE
['aaa', 'bbb', 'ccc', '']


$ cat /tmp/test.json 
{
    "hw": "Hello world!",
    "drink": [ "Milk", "Beer", "Water", "Wine", "Whiskey" ]
}

$ okerrclient --tpconf FILE:read=/tmp/test.json -s 'FILE /tmp/test.json' MULTILINE FROMJSON DUMP NONE
{u'drink': [u'Milk', u'Beer', u'Water', u'Wine', u'Whiskey'], u'hw': u'Hello world!'}

~~~
