# FORK

Одна из ключевых команд sequence. На входе берет список, и запускает отдельную обработку для каждого элемента списка, что позволяет одним скриптом обновить несколько (иногда, неизвестное заранее количество) индикаторов.

## Примеры
Данный пример одним скриптом обновит (OK) два индикатора, XXX:aaa и XXX:bbb
~~~
$ okerrclient --name XXX -s 'STR ["aaa","bbb"]' FROMJSON FORK 'NAME $_name:$_str' OK SEQDUMP NONE
Sequence name 'XXX:aaa': #140673263999600 Parent 'XXX': #140673263998088
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {}
data: (2): OK


Sequence name 'XXX:bbb': #140673263999600 Parent 'XXX': #140673263998088
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {}
data: (2): OK
~~~

В этом скрипте (lib:df) используется FORK. Он обновляет несколько индикаторов, по количеству подмонтированных файловых систем. На одном сервере это может быть один индикатор, на другом - много. Один и тот же код с FORK подходит любому серверу.
~~~
$ okerrclient -s "KEYVAL lib:df" JDUMP NONE
{
    "010": "NAME $_name:df",
    "020": "METHOD numerical maxlim=80",
    "030": "TAG df",
    "040": "DF",
    "050": "FILTER not '/media/' in path",
    "060": "FORK",
    "070": "EXPORT path=path",
    "080": "NAME $_name-$path",
    "090": "DETAILS $percent% ($usedg/${totalg}G used, $freeg left)",
    "100": "FORMAT $percent"
}
~~~


