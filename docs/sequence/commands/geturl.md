# GETURL

Получение документов по HTTP и HTTPS GET.

## Конфигурация
url (--tpconf GETURL:url=prefix) - префикс, для разрешенных документов. По-умолчанию, запрещено все (и GETURL не работает до настройки разрешения).

## Параметры

- **url**='' - адрес (URL) документа для HTTP GET.
- **redirect**='1' - если '1' - то, при получении от сервера кода редиректа, будет выполнен запрос по новому адресу. Если '0' - то будут возвращены данные для первого запроса. 
- **verify** ='1' - если '1' и запрос по HTTPS, будет выполняться HTTPS верификация. Если '0' то не будет.
- **user** = '' - имя пользователя для basic аутентификации
- **pass** = '' - пароль пользователя для basic аутентификации
- **timeout** = '5' - таймаут в секундах
 
## Примеры
~~~~
# просто смотрим доступную информацию
$ okerrclient --dry --tpconf GETURL:url=http://okerr.com -s 'GETURL url=http://okerr.com/ redirect=0' JDUMP
{
    "h_Connection": "Keep-Alive",
    "h_Content-Length": "301",
    "h_Content-Type": "text/html; charset=iso-8859-1",
    "h_Date": "Thu, 07 Sep 2017 21:05:10 GMT",
    "h_Keep-Alive": "timeout=5, max=100",
    "h_Location": "https://okerr.com/",
    "h_Server": "Apache/2.4.10 (Debian)",
    "status_code": 301,
    "text": "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html><head>\n<title>301 Moved Permanently</title>\n</head><body>\n<h1>Moved Permanently</h1>\n<p>The document has moved <a href=\"https://okerr.com/\">here</a>.</p>\n<hr>\n<address>Apache/2.4.10 (Debian) Server at okerr.com Port 80</address>\n</body></html>\n"
}

# Отчитываемся о редиректе на сервер
$ okerrclient --name test:redirect --tpconf GETURL:url=* -s 'METHOD string options="reinit"' 'GETURL url=http://okerr.com/ redirect=0' SAVE 'FORMAT $status_code:$h_Location'
okerr updated test:redirect = 301:https://okerr.com/

# Проверяем HTTP status на стороне клиента
$ okerrclient --name test:redirect --tpconf GETURL:url=* -s 'GETURL url=http://okerr.com/ redirect=0' SAVE 'EVAL $status_code == 302'
okerr updated test:redirect = ERR

$ okerrclient --name test:redirect --tpconf GETURL:url=* -s 'GETURL url=http://okerr.com/ redirect=0' SAVE 'EVAL $status_code == 301'
okerr updated test:redirect = OK

# Если не обрабатываем ошибки: ошибка возникла, обновления нет.
$ okerrclient --name test:redirect --tpconf GETURL:url=* -s 'GETURL url=http://okerr.comZZZZ/ redirect=0' SAVE 'EVAL $status_code == 301'
GETURL exception <class 'requests.exceptions.ConnectionError'> HTTPConnectionPool(host='okerr.comzzzz', port=80): Max retries exceeded with url: / (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f55feb1de10>: Failed to establish a new connection: [Errno -2] Name or service not known',))

# Корректно обрабатываем ошибки через EXCEPT
$ okerrclient --name test:redirect --tpconf GETURL:url=* -s 'EXCEPT ERR' 'GETURL url=http://okerr.comZZZZ/ redirect=0' SAVE 'EVAL $status_code == 301'
GETURL exception <class 'requests.exceptions.ConnectionError'> HTTPConnectionPool(host='okerr.comzzzz', port=80): Max retries exceeded with url: / (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f7cf1195e10>: Failed to establish a new connection: [Errno -2] Name or service not known',))
okerr updated test:redirect = ERR

~~~~


