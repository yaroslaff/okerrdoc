# KMGT
Конвертирует число (например, размер файла) в строку с суффиксом (например, 12М)

## Аргументы
	
- **destfield**=''
- **field**=''
- **frac**='1' - количество знаков после запятой

# Описание
Преобразует данные об объеме памяти в удобочитаемый вид с суффиксом (K, M, G, T) для, соответственно кило-, мега-, гига- и терабайт.

Если на входе - словарь, обрабатывается поле **field** словаря. Если указан **destfield** то вычисленное значение будет записано в это поле.

Если на входе список словарей - аналогично обрабатывается каждый словарь из списка.

Так же см. **DHMS**.


# Пример
~~~
$ okerrclient -s "STR 12345" KMGT JDUMP NONE
"12.1K"

$ okerrclient -s "STR 12345" 'KMGT frac=0' JDUMP NONE
"12K"

# original data
$ okerrclient -s "DIR path=/etc/passwd"  JDUMP NONE
[
    {
        "aage": 7006,
        "atime": 1486558099,
        "basename": "passwd",
        "cage": 14425515,
        "ctime": 1472139590,
        "depth": 0,
        "mage": 14425515,
        "mtime": 1472139590,
        "path": "/etc/passwd",
        "size": 2407,
        "type": "REG"
    }
]

# processed data
$ okerrclient -s "DIR path=/etc/passwd" "KMGT field=size"  JDUMP NONE
[
    {
        "aage": 7019,
        "atime": 1486558099,
        "basename": "passwd",
        "cage": 14425529,
        "ctime": 1472139590,
        "depth": 0,
        "mage": 14425529,
        "mtime": 1472139590,
        "path": "/etc/passwd",
        "size": "2.4K",
        "type": "REG"
    }
]

# human-readable value in new field
$ okerrclient -s "DIR path=/etc/passwd" LAST "KMGT field=size destfield=humansize"  JDUMP NONE
{
    "aage": 7121,
    "atime": 1486558099,
    "basename": "passwd",
    "cage": 14425631,
    "ctime": 1472139590,
    "depth": 0,
    "humansize": "2.4K",
    "mage": 14425631,
    "mtime": 1472139590,
    "path": "/etc/passwd",
    "size": 2407,
    "type": "REG"
}
~~~
