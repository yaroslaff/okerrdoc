# DIR

Дает информацию о файлах и каталогах.

# Аргументы

- **maxdepth**='10'
- **md5**=''
- **mindepth**='0'
- **path**='' - имя файла или каталога. Обязательный параметр.
- **sha1**=''
- **sha256**=''
- **symlink**='0'

# Описание
Рекурсивно дает информацию (stat) о файлах и каталогах.

Если **symlink**=1 - то следует по символьным ссылкам.
Если **md5**, **sha1** или **sha256** равны 1, так же высчитывает хеш для файлов, что позволяет использовать okerr в качестве простой хостовой системы обнаружения вторжений.

**mindepth** и **maxdepth** - минимальная и максимальная *глубина углубления*. (По аналогии с утилитой find)

На выходе DIR - всегда список словарей, даже если path указывает на файл. Если нужно получить словарь по одному файлу - можно добавить **FIRST** или **LAST**.

# Примеры
Подготовка:
~~~
$ mkdir /tmp/dir
$ mkdir /tmp/dir/aaa
$ mkdir /tmp/dir/bbb
$ echo one > /tmp/dir/bbb/one 
$ echo two > /tmp/dir/bbb/two
$ ln -s /tmp/dir/bbb/ /tmp/dir/aaa/b-link
~~~

Содержимое каталога:
~~~
$ okerrclient -s 'DIR path=/tmp/dir/bbb' JDUMP NONE
[
    {
        "aage": 52,
        "atime": 1486569377,
        "basename": "two",
        "cage": 52,
        "ctime": 1486569377,
        "depth": 1,
        "mage": 52,
        "mtime": 1486569377,
        "path": "/tmp/dir/bbb/two",
        "size": 4,
        "type": "REG"
    },
    {
        "aage": 57,
        "atime": 1486569371,
        "basename": "one",
        "cage": 57,
        "ctime": 1486569371,
        "depth": 1,
        "mage": 57,
        "mtime": 1486569371,
        "path": "/tmp/dir/bbb/one",
        "size": 4,
        "type": "REG"
    },
    {
        "aage": 73,
        "atime": 1486569355,
        "basename": "bbb",
        "cage": 52,
        "ctime": 1486569377,
        "depth": 0,
        "mage": 52,
        "mtime": 1486569377,
        "path": "/tmp/dir/bbb",
        "size": 4096,
        "type": "DIR"
    }
]
~~~

Только внутри каталога (ограничение mindepth):
~~~
$ okerrclient -s 'DIR path=/tmp/dir/bbb mindepth=1' JDUMP NONE
[
    {
        "aage": 94,
        "atime": 1486569377,
        "basename": "two",
        "cage": 94,
        "ctime": 1486569377,
        "depth": 1,
        "mage": 94,
        "mtime": 1486569377,
        "path": "/tmp/dir/bbb/two",
        "size": 4,
        "type": "REG"
    },
    {
        "aage": 99,
        "atime": 1486569371,
        "basename": "one",
        "cage": 99,
        "ctime": 1486569371,
        "depth": 1,
        "mage": 99,
        "mtime": 1486569371,
        "path": "/tmp/dir/bbb/one",
        "size": 4,
        "type": "REG"
    }
]
~~~

Один файл:
~~~
$ okerrclient -s 'DIR path=/tmp/dir/bbb/one' JDUMP NONE
[
    {
        "aage": 155,
        "atime": 1486569371,
        "basename": "one",
        "cage": 155,
        "ctime": 1486569371,
        "depth": 0,
        "mage": 155,
        "mtime": 1486569371,
        "path": "/tmp/dir/bbb/one",
        "size": 4,
        "type": "REG"
    }
]
$ okerrclient -s 'DIR path=/tmp/dir/bbb/one' LAST JDUMP NONE
{
    "aage": 159,
    "atime": 1486569371,
    "basename": "one",
    "cage": 159,
    "ctime": 1486569371,
    "depth": 0,
    "mage": 159,
    "mtime": 1486569371,
    "path": "/tmp/dir/bbb/one",
    "size": 4,
    "type": "REG"
}
~~~

Хешсумма:
~~~
$ okerrclient -s 'DIR path=/tmp/dir/bbb/one sha256=1' LAST JDUMP NONE
{
    "aage": 6,
    "atime": 1486569625,
    "basename": "one",
    "cage": 260,
    "ctime": 1486569371,
    "depth": 0,
    "mage": 260,
    "mtime": 1486569371,
    "path": "/tmp/dir/bbb/one",
    "sha256": "2c8b08da5ce60398e1f19af0e5dccc744df274b826abe585eaba68c525434806",
    "size": 4,
    "type": "REG"
}
~~~

Символьные ссылки:
~~~
$ okerrclient -s 'DIR path=/tmp/dir/aaa/b-link' JDUMP NONE
[
    {
        "aage": 26,
        "atime": 1486569663,
        "basename": "b-link",
        "cage": 298,
        "ctime": 1486569391,
        "depth": 0,
        "mage": 298,
        "mtime": 1486569391,
        "path": "/tmp/dir/bbb",
        "size": 13,
        "type": "LNK"
    }
]

$ okerrclient -s 'DIR path=/tmp/dir/aaa/b-link symlink=1' JDUMP NONE
[
    {
        "aage": 336,
        "atime": 1486569377,
        "basename": "two",
        "cage": 336,
        "ctime": 1486569377,
        "depth": 2,
        "mage": 336,
        "mtime": 1486569377,
        "path": "/tmp/dir/bbb/two",
        "size": 4,
        "type": "REG"
    },
    {
        "aage": 87,
        "atime": 1486569625,
        "basename": "one",
        "cage": 341,
        "ctime": 1486569371,
        "depth": 2,
        "mage": 341,
        "mtime": 1486569371,
        "path": "/tmp/dir/bbb/one",
        "size": 4,
        "type": "REG"
    },
    {
        "aage": 283,
        "atime": 1486569429,
        "basename": "bbb",
        "cage": 336,
        "ctime": 1486569377,
        "depth": 1,
        "mage": 336,
        "mtime": 1486569377,
        "path": "/tmp/dir/bbb",
        "size": 4096,
        "type": "DIR"
    }
]
~~~

Фильтрация:
~~~
# How many files we have in /var/log/ with size over 1000 MB ?
$ sudo okerrclient -s 'DIR path=/var/log' 'FILTER type=="REG" and size>1024000' LEN JDUMP NONE
2
~~~

