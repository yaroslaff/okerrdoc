# JSON

Конвертирует данные (data) в JSON строчку.

Команда во многом похожа на **JDUMP**, но **JDUMP** выдает данные на консоль для отладки, а **JSON** передает их для обработки следующим командам.

## Примеры

Обратите внимание, во втором примере используется **DUMP** (а не **JDUMP**), но мы видим данные в JSON формате, потому что они были сконвертированы командой JSON
~~~ 
$ okerrclient -s "STR hello" JSON DUMP NONE
"hello"

$ okerrclient -s "DIR path=/etc/passwd" FIRST JSON DUMP NONE
{"aage": 23327, "ctime": 1472139590, "cage": 14355379, "basename": "passwd", "depth": 0, "mtime": 1472139590, "path": "/etc/passwd", "atime": 1486471642, "type": "REG", "mage": 14355379, "size": 2407}
~~~
