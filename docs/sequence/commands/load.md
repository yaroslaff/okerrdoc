# LOAD
Загружает значение переменной в данные контекста.

## Параметр (varname)

Имя переменной

## Примечание
Так же смотрите **SAVE** и **EXPORT**. Часто может быть заменено **STR**, но в отличии от нее позволяет работать с более сложными структурами данных. (STR всегда преобразовывает данные в строку)


## Примеры
~~~
okerrclient -s DATETIME 'LOAD _HH' JDUMP NONE
"01"
$ okerrclient -s DATETIME 'STR $_HH' JDUMP NONE
"01"
$ okerrclient -s DATETIME 'STR $_HH:$_MM' JDUMP NONE
"01:08"
okerrclient -s 'DEFAULT {"name": "Sasha", "born": "14/03/1988"}' 'SAVE grey' 'LOAD grey' JDUMP NONE
{
    "born": "14/03/1988",
    "name": "Sasha"
}
~~~

