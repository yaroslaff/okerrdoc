# EXCEPT

Устанавливает данные для индикатора, которые будут отправлены если при обработке sequence-скрипта произойдет сбой. 

## Примеры

~~~
# good example, file /tmp/c exists
$ okerrclient --name 'test:filesize' -s 'DIR path=/tmp/c' LAST 'FORMAT $size'
okerr updated test:filesize = 340

# No file /tmp/c2.
$ okerrclient --name 'test:filesize' -s 'DIR path=/tmp/c2' LAST 'FORMAT $size'

# what exactly happened?
$ okerrclient -v --name 'test:filesize' -s 'DIR path=/tmp/c2' LAST 'FORMAT $size'
RUN DIR sha1= symlink=0 mindepth=0 maxdepth=10 path=/tmp/c2 sha256= md5= 
exception:[Errno 2] No such file or directory: '/tmp/c2'
sequence name:test:filesize id:139873650893888 got stop signal
do no update indicator test:filesize, because data is None

# Now we supply value for exceptional situaton
$ okerrclient --name 'test:filesize' -s 'EXCEPT 0' 'DIR path=/tmp/c2' LAST 'FORMAT $size'
okerr updated test:filesize = 0

# 42 works too
$ okerrclient --name 'test:filesize' -s 'EXCEPT 42' 'DIR path=/tmp/c2' LAST 'FORMAT $size'
okerr updated test:filesize = 42
~~~
