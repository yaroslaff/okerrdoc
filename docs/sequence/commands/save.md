# SAVE    
Сохраняет данные в переменную. Далее переменную можно использовать через подстановку ($varname) или через команду **LOAD**.

# Параметр (varname)
Имя переменной.

Если имя переменной не задано, и данные (data) - имеют тип "словарь", то каждый ключ из словаря устанавливается в переменную.

# Примеры      
~~~
$ okerrclient --dry -s 'STR bar' 'SAVE foo' SEQDUMP 
Sequence name 'braconnier': #140012541568280 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {
    "foo": "bar"
}
data: (3): bar

$ okerrclient --dry -s 'STR bar' 'SAVE foo' 'STR foo is $foo' DUMP 
foo is bar

$ okerrclient --dry -s 'STR bar' 'SAVE foo' 'STR zzz' 'LOAD foo' DUMP 
bar
~~~       

Установка переменных из словаря:
~~~
$ okerrclient --dry -s 'DIR path=/etc/fstab' FIRST SAVE SEQDUMP
Sequence name 'braconnier': #140337715622688 no parent
lastcmd: SEQDUMP
route:
details: None
except: None
method: None
tags: []
variables: {
    "aage": 11969,
    "atime": 1504803984,
    "basename": "fstab",
    "cage": 41682959,
    "ctime": 1463132994,
    "depth": 0,
    "mage": 41682959,
    "mtime": 1463132994,
    "path": "/etc/fstab",
    "size": 594,
    "type": "REG"
}

$ okerrclient --dry -s 'DIR path=/etc/fstab' FIRST SAVE 'STR $basename' JDUMP
"fstab"
~~~

