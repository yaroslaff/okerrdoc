# MULTILINE

На входе берет итерируемый объект (файл или список), превращает его в длинную строку, объединенную через '\n'.

## Примеры
~~~
$ okerrclient -s "LIST aaa bbb ccc" JDUMP NONE
[
    "aaa",
    "bbb",
    "ccc"
]

$ okerrclient -s "LIST aaa bbb ccc" MULTILINE JDUMP NONE
"aaa\nbbb\nccc"

$ cat /tmp/abc.txt 
aaa
bbb
ccc

# too many newlines. \n in each line + \n separator added
$ okerrclient --tpconf FILE:read=/tmp/abc.txt -s 'FILE /tmp/abc.txt' MULTILINE DUMP NONE
aaa

bbb

ccc

# better way
$ okerrclient --tpconf FILE:read=/tmp/abc.txt -s 'FILE /tmp/abc.txt' STRIP DUMP NONE
['aaa', 'bbb', 'ccc', '']

$ okerrclient --tpconf FILE:read=/tmp/abc.txt -s 'FILE /tmp/abc.txt' STRIP MULTILINE DUMP NONE
aaa
bbb
ccc


~~~ 
