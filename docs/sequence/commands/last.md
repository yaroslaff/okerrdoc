# LAST

Оставляет только последние несколько элементов в строке. (см. так же **FIRST**).

## Параметр (n)
Сколько элементов в конце списка нужно оставить. Если n равен 1 (по-умолчанию), то вместо списка элементов, возвращается сам этот элемент.

## Примеры
~~~
# return list of 2 strings.
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" "LAST 2"  JDUMP NONE                                
[
    "Starr",
    "Harrison"
]

# returns string, not list of 1 string!
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" "LAST"  JDUMP NONE
"Harrison"

# Largest file in /var/log
$ sudo okerrclient -s "DIR path=/var/log" "SORT field=size" LAST JDUMP NONE
{
    "aage": 56554824,
    "atime": 1429942387,
    "basename": "current_mbr.img",
    "cage": 56554724,
    "ctime": 1429942487,
    "depth": 5,
    "mage": 56554724,
    "mtime": 1429942487,
    "path": "/var/log/boot-sav/log/2015-04-25__06h12boot-repair52/sda/current_mbr.img",
    "size": 1048576,
    "type": "REG"
}
~~~
