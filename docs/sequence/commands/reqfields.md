# REQFIELDS

Фильтр для списков словарей. Оставляет только те элементы, в которых есть все требуемые поля.

## Параметр (fields)
Список требуемых полей через пробел

## Примеры


~~~
$ okerrclient -s PROCESSES LEN JDUMP NONE
300
$ okerrclient -s PROCESSES 'REQFIELDS basename' LEN JDUMP NONE
139

# Not good, because special processes like kthreadd, ksoftirqd/0, kworker, rcu_sched has no basename
$ okerrclient -s PROCESSES 'FORMAT $pid: $basename' 'FIRST 10' JDUMP NONE
[
    "1: init",
    "2: $basename",
    "3: $basename",
    "5: $basename",
    "7: $basename",
    "8: $basename",
    "9: $basename",
    "10: $basename",
    "11: $basename",
    "12: $basename"
]

# Very good!
$ okerrclient -s PROCESSES 'REQFIELDS basename' 'FORMAT $pid: $basename' 'FIRST 10' JDUMP NONE
[
    "1: init",
    "514: bluetoothd",
    "543: systemd-logind",
    "831: ModemManager",
    "933: polkitd",
    "953: cups-browsed",
    "979: wpa_supplicant",
    "1043: dnsmasq",
    "1334: winbindd",
    "1335: docker"
]


~~~
