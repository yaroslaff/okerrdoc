# LOADAVG
В детали (details) контекста записываются все 3 значения Load Average. В данные - записывается одно из них. 


## Аргумент (period)
Число (индекс) от 0 до 2, указывает, какое из 3 значений должно быть записано в данные.

## Примеры
~~~
$ okerrclient -s "LOADAVG period=2" SEQDUMP NONE
Sequence name 'braconnier': #140338877396176 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: 0.18, 0.32, 0.31
method: None
tags: []
variables: {}
data: (4): 0.31
~~~
