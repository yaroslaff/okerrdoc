# SORT

Сортирует списки из строк или структур.

Если на входе - строка, она преобразовывается (split) в список. Разделителем указывается аргументом sep (по-умолчанию - пробел). В таком случае, после обработки список снова преобразовывается в строчку (join) через пробел.

Сортировка может быть удобно для определения "хит-парадов" (например, 10 самых больших лог-файлов на диске).

## Аргументы

- **empty**='0' 
- **field**='' - при сортировке списка словарей, сортировка будет выполнена по этому полю
- **num**='0' - если 1 - то сортировка будет по численному значению, а не строковому
- **sep**=' ' - разделитель, при сортировке слов в строке





## Примеры
Сортировка слов и внутри строковых данных:
~~~
$ okerrclient -s 'STR ccc bbb aaa' SORT DUMP NONE
aaa bbb ccc
$ okerrclient -s 'STR ccc|bbb|aaa' "SORT sep=|" DUMP NONE
aaa bbb ccc
~~~

Строка (строковое значение данных) может состоять из нескольких строчек:
~~~
$ okerrclient -s 'DEFAULT "ccc\nbbb\naaa"' DUMP NONE
ccc
bbb
aaa
$ okerrclient -s 'DEFAULT "ccc\nbbb\naaa"' 'SORT sep=\\n' DUMP NONE
aaa bbb ccc
~~~

Сортировка по числовому значению:
~~~
$ okerrclient -s 'STR 1000 33 2' SORT DUMP NONE
1000 2 33
$ okerrclient -s 'STR 1000 33 2' "SORT num=1" DUMP NONE
2 33 1000
~~~

Сортировка списка строк:
~~~
$ okerrclient -s PROCESSES 'FORMAT $name' JDUMP NONE | head
[
    "init",
    "kthreadd",
    "ksoftirqd/0",
    "kworker/0:0H",
    "rcu_sched",
    "rcuos/0",
    "rcuos/1",
    "rcuos/2",
    "rcuos/3",
$ okerrclient -s PROCESSES 'FORMAT $name' SORT JDUMP NONE | head
[
    "ModemManager",
    "NetworkManager",
    "Xorg",
    "accounts-daemon",
    "acpid",
    "apache2",
    "apache2",
    "apache2",
    "apache2",
~~~

Сортировка списка словарей:
~~~
$ sudo okerrclient -s CONNECTIONS "SORT field=basename" JDUMP NONE| head
[
    {
        "basename": "apache2",
        "exe": "/usr/sbin/apache2",
        "ip": "::",
        "name": "apache2",
        "pid": 2444,
        "port": 80,
        "proto": "TCP6",
        "status": "LISTEN"
~~~

Полезный пример - найти один лог-файл с максимальным размером:
~~~
$ sudo okerrclient -s "DIR path=/var/log maxdepth=1" "SORT field=size" "LAST 1" JDUMP NONE
{
    "aage": 166764,
    "atime": 1486230210,
    "basename": "syslog.1",
    "cage": 10074,
    "ctime": 1486386900,
    "depth": 1,
    "mage": 10078,
    "mtime": 1486386896,
    "path": "/var/log/syslog.1",
    "size": 451076,
    "type": "REG"
}
~~~


