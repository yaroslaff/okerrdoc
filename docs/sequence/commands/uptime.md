# UPTIME
Возвращает uptime системы в секундах.

Если аптайм понизился - это обычно является признаком того, что между измерениями системы прошел ребут.

Для превращения секунд в удобочитаемый формат удобно пользоваться **DHMS**

# Примеры
~~~
$ okerrclient -s UPTIME DUMP NONE
883136

$ okerrclient -s "KEYVAL lib:uptime" JDUMP NONE
{
    "010": "NAME $_name:uptime",
    "020": "METHOD numerical diffmin=0",
    "030": "TAG uptime",
    "040": "UPTIME",
    "050": "SAVE uptimesec",
    "060": "DHMS",
    "070": "DETAILS",
    "080": "LOAD uptimesec"
}

$ okerrclient -s UPTIME DHMS DUMP NONE
10d 5h

~~~
