# FORMAT

Превращает словарь (dict) в строку по шаблону. Если на входе - список словарей, то операция производится над каждым элементом (создается список строк).


## Параметр (format)
Параметром задается строка шаблона. $подстроки будут заменены на значение полей словаря.

## Примеры
~~~
$ okerrclient -s "DIR path=/etc/passwd sha1=1" FIRST JDUMP NONE
{
    "aage": 20821,
    "atime": 1486384982,
    "basename": "passwd",
    "cage": 14266214,
    "ctime": 1472139590,
    "depth": 0,
    "mage": 14266214,
    "mtime": 1472139590,
    "path": "/etc/passwd",
    "sha1": "fc70b8cec51997dc0b11b6ead634f91d56234b8c",
    "size": 2407,
    "type": "REG"
}

$ okerrclient -s "DIR path=/etc/passwd sha1=1" FIRST 'FORMAT $path hashsum is $sha1' JDUMP NONE
"/etc/passwd hashsum is fc70b8cec51997dc0b11b6ead634f91d56234b8c"

$ okerrclient -s PROCESSES 'FIRST 3' JDUMP NONE
[
    {
        "basename": "init",
        "exe": "/sbin/init",
        "name": "init",
        "pid": 1,
        "username": "root"
    },
    {
        "exe": null,
        "name": "kthreadd",
        "pid": 2,
        "username": "root"
    },
    {
        "exe": null,
        "name": "ksoftirqd/0",
        "pid": 3,
        "username": "root"
    }
]
$ okerrclient -s PROCESSES 'FIRST 3' 'FORMAT pid: $pid exe: $exe' JDUMP NONE
[
    "pid: 1 exe: /sbin/init",
    "pid: 2 exe: None",
    "pid: 3 exe: None"
]
~~~

## Примечание
Не забывайте о том, что оболочка (шелл) сама подменяет $-подстроки на значение переменных окруженгия

~~~
$ okerrclient -s PROCESSES 'FIRST 3' "FORMAT pid: $pid exe: $exe" JDUMP NONE    ### Do not do this! :-(
[
    "pid:  exe: ",
    "pid:  exe: ",
    "pid:  exe: "
]
$ okerrclient -s PROCESSES 'FIRST 3' "FORMAT pid: \$pid exe: \$exe" JDUMP NONE  ### Do this! :-)
[
    "pid: 1 exe: /sbin/init",
    "pid: 2 exe: None",
    "pid: 3 exe: None"
]
$ okerrclient -s PROCESSES 'FIRST 3' 'FORMAT pid: $pid exe: $exe' JDUMP NONE    ### Or do this! :-)
[
    "pid: 1 exe: /sbin/init",
    "pid: 2 exe: None",
    "pid: 3 exe: None"
]
~~~

Так же не забывайте, что процессор okerrclient так же *сначала* выполняет подстановки переменных, а уже потом обработанную строчку передает команде. Например, если у нас есть переменная pid, которая равна "PID_VARIABLE", то FORMAT получит строчку "pid: PID_VARIABLE exe: $exe" ($pid заменено на значение переменной, $exe не заменено, так как такой переменной в контексте нет)

~~~
$ okerrclient -s 'SET pid=PID_VARIABLE' PROCESSES 'FIRST 3' "FORMAT pid: \$pid exe: \$exe" JDUMP NONE
[
    "pid: PID_VARIABLE exe: /sbin/init",
    "pid: PID_VARIABLE exe: None",
    "pid: PID_VARIABLE exe: None"
]
~~~


