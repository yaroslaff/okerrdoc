# LIST

Создание списка на основе параметра или данных

## Параметр (list)
Если параметр указан - то будет создан список на основе значений из параметра. В качестве сепаратора используется символ пробела.

Если параметра нет, то генерируется список на основе data из контекста, они должны быть итерируемы, например, открытый файл.

# Примеры
~~~
$ okerrclient -s "STR foo bar baz" JDUMP NONE
"foo bar baz"
$ okerrclient -s "LIST foo bar baz" JDUMP NONE
[
    "foo",
    "bar",
    "baz"
]
$ okerrclient -s 'LIST "foo bar" baz' JDUMP NONE
[
    "foo bar",
    "baz"
]
~~~

Чтение списка из файла:
~~~
$ cat /tmp/123.txt 
one
two
three
$ okerrclient --tpconf FILE:read=/tmp/123.txt -s "FILE /tmp/123.txt" DUMP NONE
<open file '/tmp/123.txt', mode 'r' at 0x7f0c83583420>
$ okerrclient --tpconf FILE:read=/tmp/123.txt -s "FILE /tmp/123.txt" LIST DUMP NONE
['one\n', 'two\n', 'three\n']
$ okerrclient --tpconf FILE:read=/tmp/123.txt -s "FILE /tmp/123.txt" LIST STRIP DUMP NONE
['one', 'two', 'three']
~~~

