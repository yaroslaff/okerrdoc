# OK
Просто возвращает строку "OK". Для установки ошибки на простых (heartbeat) индикаторах. 

Эквивалентно STR "OK"

Так же, по аналогии работает команда **ERR**

## Примеры
~~~
$ okerrclient -s OK DUMP NONE
OK
$ okerrclient -s 'STR OK' DUMP NONE
OK
~~~


