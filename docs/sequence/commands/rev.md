# REV
Переворачивает список

## Примеры
~~~
$ okerrclient -s "LIST one two three" JDUMP NONE
[
    "one",
    "two",
    "three"
]
$ okerrclient -s "LIST one two three" REV JDUMP NONE
[
    "three",
    "two",
    "one"
]
~~~

Самый большой/маленький файл:
~~~
# Largest file will be last in list
$ sudo okerrclient -s "DIR path=/var/log" "FILTER type=='REG'" "SORT field=size" LAST JDUMP NONE
{
    "aage": 56629247,
    "atime": 1429942387,
    "basename": "current_mbr.img",
    "cage": 56629147,
    "ctime": 1429942487,
    "depth": 5,
    "mage": 56629147,
    "mtime": 1429942487,
    "path": "/var/log/boot-sav/log/2015-04-25__06h12boot-repair52/sda/current_mbr.img",
    "size": 1048576,
    "type": "REG"
}

# Reverse list. Now smallest file will be last in last
$ sudo okerrclient -s "DIR path=/var/log" "FILTER type=='REG'" "SORT field=size" REV LAST JDUMP NONE
{
    "aage": 1644276,
    "atime": 1484927362,
    "basename": "mysql.err",
    "cage": 1644276,
    "ctime": 1484927362,
    "depth": 1,
    "mage": 1644276,
    "mtime": 1484927362,
    "path": "/var/log/mysql.err",
    "size": 0,
    "type": "REG"
}
~~~
