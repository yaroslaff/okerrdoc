# ONLYFIELDS

Удаляет все поля из списка словарей кроме указанных.


## Параметр (fields)
Список полей, которые нужно оставить (через пробел). 

## Пример
~~~
$ sudo okerrclient -s "DIR path=/var/log" "FIRST 1" JDUMP NONE
{
    "aage": 8521137,
    "atime": 1477973616,
    "basename": "pm-suspend.log.3.gz",
    "cage": 584513,
    "ctime": 1485910240,
    "depth": 1,
    "mage": 5931241,
    "mtime": 1480563512,
    "path": "/var/log/pm-suspend.log.3.gz",
    "size": 8581,
    "type": "REG"
}
 
$ sudo okerrclient -s "DIR path=/var/log" "ONLYFIELDS size basename" "FIRST 1" JDUMP NONE
{
    "basename": "pm-suspend.log.3.gz",
    "size": 8581
}
~~~
