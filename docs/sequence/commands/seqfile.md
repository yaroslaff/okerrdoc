# SEQFILE

Заспускает скрипт из текстового файла

## Параметр (path)
Имя файла. Файл должен быть разрешен для чтения через --tpconf FILE:read.

## Описание
SEQFILE - "алиас" для трех команд: ['FILE', 'LIST', 'MKSEQ'], для более удобного использования

## Примеры
~~~
# our test script
$ cat /tmp/script.seq 
NAME test
OK

# hard way (dont try to understand it at home)
$ okerrclient --tpconf FILE:read=/tmp/script.seq -s "FILE /tmp/script.seq" "LIST" MKSEQ
okerr updated test = OK

# SEQFILE makes it easy
$ okerrclient --tpconf disable -s "SEQFILE /tmp/script.seq"
okerr updated test = OK
~~~
