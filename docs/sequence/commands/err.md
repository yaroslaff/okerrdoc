# ERR
Просто возвращает строку "ERR". Для установки ошибки на простых (heartbeat) индикаторах. 

Эквивалентно STR "ERR"

Так же, по аналогии работает команда **OK**

## Примеры
~~~
$ okerrclient -s ERR DUMP NONE
ERR
$ okerrclient -s 'STR ERR' DUMP NONE
ERR
~~~

