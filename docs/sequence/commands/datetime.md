# DATETIME 

Устанавливает переменные даты и времени.

## Аргументы

**offset**='0' Смещение в секундах. (offset=86400 даст время завтрашнего дня)

**prefix**='_' Префикс, который будет добавлен к переменным. 


## Примеры

~~~
$ okerrclient -s DATETIME SEQDUMP NONE
Sequence name 'braconnier': #140168341447960 no parent
lastcmd: SEQDUMP
route:
	NONE 
details: None
method: None
tags: []
variables: {
    "_HH": "00",
    "_MM": "24",
    "_SS": "59",
    "_day": 3,
    "_dd": "03",
    "_hour": 0,
    "_minute": 24,
    "_mm": "02",
    "_month": 2,
    "_second": 59,
    "_year": 2017,
    "_yy": "17"
}
no data

$ okerrclient -s DATETIME 'STR Now $_HH:$_MM' DUMP NONE
Now 00:27
~~~

