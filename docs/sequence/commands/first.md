# FIRST

Оставляет только первые несколько элементов в строке. (см. так же **LAST**).

## Параметр (n)
Сколько элементов в конце списка нужно оставить. Если n равен 1 (по-умолчанию), то вместо списка элементов, возвращается сам этот элемент.

## Примеры
~~~
# return list of 2 strings.
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" "FIRST 2"  JDUMP NONE
[
    "Lennon",
    "McCartney"
]

# returns string, not list of 1 string!
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" FIRST  JDUMP NONE
"Lennon"

# Smallest file in /var/log
$ sudo okerrclient -s "DIR path=/var/log" "SORT field=size" FIRST JDUMP NONE
{
    "aage": 1639713,
    "atime": 1484927362,
    "basename": "mysql.err",
    "cage": 1639713,
    "ctime": 1484927362,
    "depth": 1,
    "mage": 1639713,
    "mtime": 1484927362,
    "path": "/var/log/mysql.err",
    "size": 0,
    "type": "REG"
}

# Largest file in /var/log
$ sudo okerrclient -s "DIR path=/var/log" "SORT field=size" REV FIRST JDUMP NONE
{
    "aage": 56624700,
    "atime": 1429942387,
    "basename": "current_mbr.img",
    "cage": 56624600,
    "ctime": 1429942487,
    "depth": 5,
    "mage": 56624600,
    "mtime": 1429942487,
    "path": "/var/log/boot-sav/log/2015-04-25__06h12boot-repair52/sda/current_mbr.img",
    "size": 1048576,
    "type": "REG"
}
~~~

