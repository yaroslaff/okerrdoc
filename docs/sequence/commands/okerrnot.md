# OKERRNOT        

Инвертирует результат. Если на входе строка "OK" - возвращает "ERR". Если на входе строка "ERR" - возвращает "OK".
Если на входе любые другие данные - возвращает "ERR".

~~~
$ okerrclient -s OK OKERRNOT DUMP NONE
ERR
$ okerrclient -s ERR OKERRNOT DUMP NONE
OK
$ okerrclient -s 'STR abc' OKERRNOT DUMP NONE
ERR
$ okerrclient -s OKERRNOT DUMP NONE
ERR
~~~
