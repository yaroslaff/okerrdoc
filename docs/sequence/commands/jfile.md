# JFILE

Считывает данные из файла в формате JSON

## Параметр (path)
Имя файла. Файл должен быть разрешен для чтения через --tpconf FILE:read.

## Описание
JFILE - "алиас" для трех команд: ['FILE', 'MULTILINE', 'FROMJSON'], для более удобного использования

## Примеры
~~~

# our test data
$ cat /tmp/test.json 
{
    "hw": "Hello world!",
    "drink": [ "Milk", "Beer", "Water", "Wine", "Whiskey" ]
}

# JFILE, easy way
$ okerrclient --tpconf FILE:read=/tmp/test.json -s "JFILE /tmp/test.json" JDUMP NONE
{
    "drink": [
        "Milk",
        "Beer",
        "Water",
        "Wine",
        "Whiskey"
    ],
    "hw": "Hello world!"
}

# Hard way
$ okerrclient --tpconf FILE:read=/tmp/test.json -s "FILE /tmp/test.json" MULTILINE FROMJSON JDUMP NONE
{
    "drink": [
        "Milk",
        "Beer",
        "Water",
        "Wine",
        "Whiskey"
    ],
    "hw": "Hello world!"
}
~~~
