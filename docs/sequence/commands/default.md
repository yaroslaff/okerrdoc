# DEFAULT 
Устанавливает данные по умолчанию, если в контексте данных нет.

## Параметр (def)

Строка с данными в JSON представлении. По-умолчанию - "".

## Примеры

~~~
$ okerrclient -s 'DEFAULT "alpha"' JDUMP NONE
"alpha"

$ okerrclient -s 'STR hello!' 'DEFAULT "alpha"' JDUMP NONE
"hello!"

$ okerrclient -s 'DEFAULT {"name": "Sasha", "born": "14/03/1988"}' JDUMP NONE
{
    "born": "14/03/1988",
    "name": "Sasha"
}
~~~

