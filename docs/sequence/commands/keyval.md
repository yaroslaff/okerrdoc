
# KEYVAL
Забирает данные из серверной базы ключей.

## Параметр (path)

путь в базе ключей

## Примечание
Для доступа необходимы валидные значения textid, keyuser, keypass, которые надо указать в файле конфигурации либо через параметры --textid, --keyuser, --keypass

## Примеры
~~~
$ okerrclient -s KEYVAL JDUMP NONE # no path, so download FULL key-value database

$ okerrclient -s 'KEYVAL conf:anyserver:uptime' JDUMP NONE # How does uptime implemented?
{
    "010": "NAME $_name:uptime",
    "020": "METHOD numerical diffmin=0",
    "030": "TAG uptime",
    "040": "UPTIME",
    "050": "SAVE uptimesec",
    "060": "DHMS",
    "070": "DETAILS",
    "080": "LOAD uptimesec"
}

~~~

