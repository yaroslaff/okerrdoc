# JDUMP

Отладочная команда. Печатает содержимое данных (data) контекста в pretty JSON формате.

# Примеры
~~~
$ okerrclient -s "STR abc" JDUMP NONE
"abc"

$ okerrclient -s DF JDUMP NONE
[
    {
        "free": 257691041792,
        "freeg": 239,
        "inodes_free": 28232336,
        "inodes_percent": 5,
        "inodes_total": 29974528,
        "path": "/",
        "percent": 43.8,
        "total": 483175751680,
        "totalg": 449,
        "used": 200917151744,
        "usedg": 187
    }
]
~~~
