# NONZERO Returns OK if input is number higher then 0, or returns ERR otherwise

Возвращает OK, если число в данных больше нуля. Иначе - ERR.

На входе - требуется число или строку, представимую в число. Если это условие нарушается, выдается сообщение об ошибке и возвращается OK. 

Если на входе могут быть пустые данные, лучше перед использованием **NONZERO** использовать **DEFAULT**.

~~~
$ okerrclient -s 'STR 42' NONZERO DUMP NONE
OK
$ okerrclient -s 'STR 36.6' NONZERO DUMP NONE
OK
$ okerrclient -s 'STR 0' NONZERO DUMP NONE
ERR
$ okerrclient -s 'STR ' NONZERO DUMP NONE
bad data for NONZERO: None (must be numerical)
ERR
$ okerrclient -s 'STR " "' NONZERO DUMP NONE
bad data for NONZERO: " " (must be numerical)
ERR
$ okerrclient -s NONZERO DUMP NONE
bad data for NONZERO: None (must be numerical)
ERR
$ okerrclient -s 'DEFAULT 0' NONZERO DUMP NONE
ERR
~~~
