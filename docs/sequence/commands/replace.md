# REPLACE

Замена по регулярным выражениям.

## Аргументы
	
- **dest**=''
- **field**=''
- **replace**=''
- **search**=''

## Описание
Заменяет содержимое подстроки. Ищется шаблон (regex) **search** и заменяется на **replace**.

Если на входе - строка, выполняется эта замена в строке. Если список строк - то в каждой строке из списка.
Если список словарей - то в поле **field** каждого словаря. Если указан **dest**, то новое значение будет записано в это поле. Если не указан, то в **field** (в то же, где и был поиск шаблона).


## Примеры

Замена в строке:
~~~
$ okerrclient -s "STR Lennon McCartney Starr Harrison" 'REPLACE search=Lennon replace=Lenin' JDUMP NONE
"Lenin McCartney Starr Harrison"
~~~

Замена в списке строк:
~~~
$ okerrclient -s "LIST Lennon McCartney Starr Harrison" 'REPLACE search=rr replace=RRRRR' JDUMP NONE
[
    "Lennon",
    "McCartney",
    "StaRRRRR",
    "HaRRRRRison"
] 
~~~

Замена в списке из словарей (для наглядности использован FIRST, чтобы оставить только 1 элемент).
~~~
$ sudo ./client -s "DIR path=/var/log" 'REPLACE field=path search=log replace=LOG' FIRST JDUMP NONE
{
    "aage": 8522520,
    "atime": 1477973616,
    "basename": "pm-suspend.log.3.gz",
    "cage": 585896,
    "ctime": 1485910240,
    "depth": 1,
    "mage": 5932624,
    "mtime": 1480563512,
    "path": "/var/LOG/pm-suspend.LOG.3.gz",
    "size": 8581,
    "type": "REG"
}
~~~

Замена по регулярному выражению (последовательность от 2 до 10 цифр, будет заменена на SECRET):
~~~
$ okerrclient -s "LIST Lennon McCartney Starr Harrison 1234" 'REPLACE search=\\d{2,10} replace=SECRET' JDUMP NONE
[
    "Lennon",
    "McCartney",
    "Starr",
    "Harrison",
    "SECRET"
]
~~~

