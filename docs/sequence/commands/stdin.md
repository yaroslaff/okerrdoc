# STDIN

Читает данные с STDIN (до EOF) и помещает их в data контекста в виде строки.

## Пример
~~~
$ cat /tmp/abc.txt 
aaa
bbb
ccc

$ cat /tmp/abc.txt | okerrclient -s STDIN JDUMP NONE
"aaa\nbbb\nccc\n\n"
~~~
