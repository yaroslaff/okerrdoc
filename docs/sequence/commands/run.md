# RUN

Исполняет внешние команды.

## Параметр (prog)
Имя команды с аргументами

## Конфигурация

* safebin (--tpconf RUN:safebin=...): []
* user (--tpconf RUN:user=...): nobody

## Описание
Исполняет внешнюю программу с параметрами. Из соображений безопасности, так как код программы поступает по сети с сервера и не является абсолютно доверенным, имя программы должно быть разрешено для запуска через команду --tpconf RUN:safebin. Если в RUN:safebin будет '*' - это разрешает запуск любых программ. 

Перед исполнением, выполняется su в пользователя nobody (это можно изменить командой --tpconf RUN:user=username). Соответственно, для этого требуется запускать okerrclient от root (или через sudo). Либо установить RUN:user в имя активного пользователя.

На выходе получается структура. **code** - код исполнения процесса, **stdout**, **stderr** - содержание этих потоков. 


## Примеры

Hello world!
~~~
$ sudo okerrclient --tpconf RUN:safebin=/bin/echo -s "RUN /bin/echo hello world!" JDUMP NONE
{
    "code": 0,
    "stderr": "",
    "stdout": "hello world!\n"
}

$ sudo okerrclient --tpconf RUN:safebin=/usr/bin/uptime -s "RUN /usr/bin/uptime" 'FORMAT UPTIME: $stdout' JDUMP NONE
"UPTIME:  01:31:07 up 9 days,  8:23,  7 users,  load average: 0.44, 0.32, 0.28\n"
~~~

Использование без sudo (необходимо установить RUN:user в имя активного пользователя (оно обычно доступно в $USER), чтобы okerrclient не менял пользователя):
~~~
$ okerrclient --tpconf RUN:user=$USER RUN:safebin=/usr/bin/id -s "RUN /usr/bin/id" JDUMP NONE
{
    "code": 0,
    "stderr": "",
    "stdout": "uid=1000(xenon) gid=1000(xenon) groups=1000(xenon),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lpadmin),124(sambashare),126(wireshark),1006(ansible),1007(nagios)\n"
}
~~~

Разрешение на запуск любой программы (RUN:safebin=*). Не делайте так! 
~~~
$ okerrclient --tpconf RUN:user=$USER RUN:safebin=* -s "RUN /usr/bin/id" JDUMP NONE
{
    "code": 0,
    "stderr": "",
    "stdout": "uid=1000(xenon) gid=1000(xenon) groups=1000(xenon),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lpadmin),124(sambashare),126(wireshark),1006(ansible),1007(nagios)\n"
}
~~~

