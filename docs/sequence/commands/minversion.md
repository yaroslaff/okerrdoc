# MINVERSION

Завершает исполнение цепочки команд, если версия okerrclient менее требуемой. Если ранее установлено EXCEPT-значение, то оно будет
 установлено для индикатора. Если не установлено - то будет использовано None (то есть, индикатор не будет установлен). Если же версия okerrclient равна требуемой или более высокая, исполнение продолжится без изменений.

Рекомендуется использовать для обработки ошибок при создании новых серверных скриптов, которые требуют новые версии okerrclient если нет
уверенности, что все машины в проекте обновили okerrclient.

Аргументом является минимальная версия в формате major.minor.subversion (три целых числа разделенных точкой).

## Примеры
~~~
# Good example, just set OK if okerrclient version higher then 0.0.0
$ okerrclient --name test -s 'MINVERSION 0.0.0' 'OK'
okerr updated (200 OK) test@yaroslaff = OK

# This example will fail and will not set OK if version less then 10.0.0
$ okerrclient --name test -s 'MINVERSION 10.0.0' 'OK'

# And here it will use EXCEPT value 'ERR' if version is too old
$ okerrclient --name test -s 'EXCEPT ERR' 'MINVERSION 10.0.0' 'OK'
okerr updated (200 OK) test@yaroslaff = ERR

# String values are fine too (if indicator has string type)
$ okerrclient --name test -s 'EXCEPT Please upgrade okerrclient' 'MINVERSION 10.0.0' 'OK'
okerr updated (200 OK) test@yaroslaff = Please upgrade okerrclient
~~~
