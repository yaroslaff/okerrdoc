# Использование python модуля okerrupdate
При установке okerrclient, на систему так же устанавливается python модуль okerrupdate. Этот вариант предпочтительнее для python-скриптов.

Пример использования:
~~~~~
#!/usr/bin/python
import okerrupdate

# create okerr project
op = okerrupdate.OkerrProject('mytextid', secret='MySecret')

# verbosity
op.verbose()

# create indicator
i = op.indicator("test:1", method='numerical|maxlim=37')
i.update('36.6', 'Current temperature is normal')
~~~~~


Эта программа будет обновлять индикаторы test:up и test:random. 

Автоматически выполняется троттлинг - то есть, даже если убрать задержку и вызывать метод update() много раз в секунду, обновления на сервер будут уходить с периодом раз в 5 минут (кроме ситуации, когда индикатор переходит из состояния OK в ERR или наоборот). Допускается обновлять индикаторы чаще, чем их период, но все таки, при значительном превышении запроса (DoS), IP может быть заблокировать. Троттлинг позволяет не беспокоиться об этой проблеме, и запускать i.update() как угодно часто.


