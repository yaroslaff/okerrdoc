# DNSBL

Проверяет наличие хоста сразу во множестве черных списков.

## Аргументы

- host: имя хоста или IP адрес, который мы проверяем
- skip: перечень DNSBL зон, в которых не нужно проверять (например, хост в них числится, мы это знаем, но нам важно узнать, если вдруг он попадет в другие черные списки). Перечислить через запятые и/или пробелы.
- extra: перечень дополнительных DNSBL зон, в которых нужно проверять. (на случай, если они не проверяются штатной проверкой).

Как правило, достаточно просто указать имя хоста и оставить skip и extra пустыми.

## Описание

В случае если хост не найден ни в одном черном списке из проверяемых (то есть, все встроенные, кроме тех, что исключены параметром skip, а так же все дополнительные, указанные в extra) - индикатор будет в статусе OK. Если он найден хотя бы в одном черном списке, индикатор будет в статусе ERR, а в деталях будут указаны несколько зон, в которых он найден. Например: `spam.dnsbl.sorbs.net (total: 1/56)`

## Список проверяемых зон

~~~
        bl_zones = [
            'cbl.abuseat.org',
            'dnsbl.sorbs.net',
            'dul.dnsbl.sorbs.net',
            'smtp.dnsbl.sorbs.net',
            'spam.dnsbl.sorbs.net',
            'zombie.dnsbl.sorbs.net',
            'sbl.spamhaus.org',
            'zen.spamhaus.org',
            'psbl.surriel.com',
            'dnsbl.njabl.org',
            'rbl.spamlab.com',
            'noptr.spamrats.com', 
            'cbl.anti-spam.org.cn', 
            'dnsbl.inps.de', 
            'httpbl.abuse.ch', 
            'short.rbl.jp', 
            'spamrbl.imp.ch', 
            'virbl.bit.nl', 
            'dsn.rfc-ignorant.org', 
            'opm.tornevall.org', 
            'multi.surbl.org', 
            'tor.dan.me.uk', 
            'relays.mail-abuse.org', 
            'rbl-plus.mail-abuse.org', 
            'access.redhawk.org', 
            'rbl.interserver.net', 
            'bogons.cymru.com', 
            'truncate.gbudb.net', 
            'bl.spamcop.net', 
            'b.barracudacentral.org', 
            'http.dnsbl.sorbs.net', 
            'misc.dnsbl.sorbs.net', 
            'socks.dnsbl.sorbs.net', 
            'web.dnsbl.sorbs.net', 
            'pbl.spamhaus.org', 
            'xbl.spamhaus.org', 
            'bl.spamcannibal.org', 
            'ubl.unsubscore.com', 
            'combined.njabl.org', 
            'dyna.spamrats.com', 
            'spam.spamrats.com', 
            'cdl.anti-spam.org.cn', 
            'drone.abuse.ch', 
            'korea.services.net', 
            'virus.rbl.jp', 
            'wormrbl.imp.ch', 
            'rbl.suresupport.com', 
            'spamguard.leadmon.net', 
            'netblock.pedantic.org', 
            'ix.dnsbl.manitu.net', 
            'rbl.efnetrbl.org', 
            'blackholes.mail-abuse.org', 
            'dnsbl.dronebl.org', 
            'db.wpbl.info', 
            'query.senderbase.org', 
            'csi.cloudmark.com',
        ]
~~~
