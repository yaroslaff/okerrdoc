# HTTP STATUS

Проверка кода HTTP status

## Аргументы

- **url** - адрес документа
- **status** - статус, который требуется у документа
- **options** - Опции (ssl_noverify, addr=), описаны в разделе "Общие аргументы".

## Описание

Выполняется запрос по указанному адресу, если код статуса HTTP соответствует тому, что указано в аргументе **status**, то индикатор устанавливается в статус OK. Иначе, в статус ERR.

Проверяется непосредственно код статуса при запросе документа точно по указанному URL. Если в ответе от сервера будет, например, код 301 (redirect) на другой адрес, то другой адрес запрашиваться не будет, а индикатор будет сверять, что полученный код 301 соответствует тому, что указано в **status**.

## Аргумент Host
Если указан аргумент "host", тогда при выполнении HTTP запроса в заголовке Host будет использовано указанное значение. Например,
указан url ``http://example.com/test`` и host ``google.com``. Будет выполнен запрос на адрес сервера ``example.com``, запрошен будет документ `/test`, поле Host в запросе будет ``google.com``.

Если аргумент host не указан - поле Host в запросе будет заполнено автоматически на основании данных из URL.

В большинстве случаев, заполнять поле host не требуется. Это может быть нужно тогда, когда нужно проверить сервер, который должен обслуживать доменное имя, но при этом DNS имя на момент проверки может указывать на адрес другого сервера. (Например, для реализации failover схемы через dynamic DNS).

