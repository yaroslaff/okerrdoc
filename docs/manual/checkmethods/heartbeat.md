# Heartbeat

Heartbeat (сердцебиение) - основной и самый простой тип индикаторов, "рабочая лошадка" в okerr. Представляет из себя простой 'watchdog'. 

Индикатор принимает состояние, которое передано с клиента. Если от клиента долго нет никаких сигналов, индикатор переходит в состояние ERR.

## Аргументы
**Secret** и **Patience** - общие аргументы для всех пассивных индикаторов.

Собственных аргументов у heartbeat нет.

## Примеры

Команды ниже установят индикатор в состояние OK и ERR. Если индикатора с этим именем нет, он будет создан. При создании индикатора, по умолчанию используется тип heartbeat.

~~~
# set to OK
$ okerrclient --name test:heartbeat -s OK
okerr updated test = OK

# set to ERR
$ okerrclient --name test:heartbeat -s ERR
okerr updated test = ERR
~~~


