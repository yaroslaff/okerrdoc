# Использование API через okerrclient

## Преимущества использования API через okerrclient
API со временем может меняться. Современная версия okerrclient и реализации API в нем - всегда будет соответствовать актуальной версии API.
okerrclient так же скрывает мелкие технические сложности, например, где нужно, сам выполняет определение нужного сервера okerr перед выполнением запроса.

## Опции
~~~
  --api-url API_URL     specify API URL (optional!)
  --api-user API_USER   okerr username
  --api-pass API_PASS   okerr password
~~~
`api-url` - адрес одного из серверов okerr, который обрабатывает данный проект. Если не указать, то okerrclient сам автоматически его определит. Так же можно задать через переменную окружения OKERR_API_URL.

`api-user` и `api-pass` можно устанавливать как через эти опции командной строки, так и через переменные окружения OKERR_API_USER и OKERR_API_PASS. Нужно указывать те user/pass, которые используются при входе в okerr (то есть, email адрес пользователя и его пароль).


Например:
~~~~
$ export OKERR_API_USER=user@example.com
$ export OKERR_API_PASS="12345"
$ okerrclient --api-indicators
~~~~
Если требуется textid проекта, он указывается через опции -i/-textid или через файл конфигурации.

**Для сокращения примеров далее мы будем полагать, что переменные OKERR_API_USER и OKERR_API_PASS установлены, а textid берется из файла конфигурации**

## Пользовательская часть API (чтение)

### Получение сервера для операций
(Для проекта по умолчанию, из конфигурации)
~~~
$ okerrclient --api-director
https://charlie.okerr.com/
~~~
или с прямым указанием textid:
~~~
$ okerrclient --api-director okerr
https://charlie.okerr.com/
~~~

Для любой отдельной операции (например, получение или изменение данных по индикатору), okerrclient сам выполнит эту операцию, если она нужна, но если требуется выполнить много операций, то проще сначала получить имя сервера и затем его использовать, это ускорит работу, например:
~~~~~
$ okerrclient --api-indicator --name test:up  # Makes two API requests
$ export OKERR_API_URL=`okerrclient --api-director`
$ okerrclient --api-indicator --name test:up  # Makes one API request instead of two
~~~~~

### Получение списка индикаторов
~~~
$ okerrclient --api-indicators
~~~
или с указанием префикса:
~~~
$ okerrclient --api-indicators test
test:random
test:up
~~~

###  Получение информации по индикатору
~~~~
$ okerrclient --api-indicator --name test:up
{
    "age": 974,
    "args": {
        "patience": "",
        "secret": ""
    },
    "changed": 1515932644,
...
~~~~

### Получение списка индикаторов с определенными параметрами
Для фильтрации по меткам используется ключ --api-filter. Для указания, что нам нужны индикаторы без признака, перед признаком надо поставить восклицательный знак. Так как в шелле этот символ служебный, то весь признак надо заключить в одинарные кавычки

~~~~
$ okerrclient --api-filter sslcert '!silent' host=okerr.com

~~~~

### Получение значения аргумента индикатора
Метод api-indicator дает больше информации, но если нужен только один аргумент:
~~~~
$ okerrclient --name ssl:cp.okerr.com --api-getarg days
20
~~~~

### Получение информации о доступных методах проверки
~~~~
$ okerrclient --api-checkmethods
~~~~

## Пользовательская часть API (чтение)

### Создать индикатор
Индикатор будет создан со всеми параметрами по умолчанию.
~~~~
$ okerrclient --name test:new --api-create
Created test:new (OK)
~~~~

Или можно сразу указать их:
~~~
$ okerrclient --name test:new2 --api-create --api-set checkmethod=sslcert --api-setarg days=20 host=okerr.com
Created test:new2 (OK)
Changed test:new2 {'checkmethod': u'sslcert'}
Changed: {u'host': u'okerr.com', u'days': u'20'}
~~~

### Удалить индикатор
~~~
$ okerrclient --name test:new2 --api-delete
Deleted indicator test:new2 from project okrrdm
~~~

### Изменить индикатор
Для изменения аргументов (которые использует метод проверки, такие как host, например), используется команда --api-setarg.
Для изменения опций индикаторов (например, метод проверки, флаги silent, maintenance, итд ) - --api-setarg

~~~
$ okerrclient --name test:new --api-set maintenance=1
Changed test:new {'maintenance': True}

$ okerrclient --name test:new --api-setarg secret=mysecret
Changed: {u'secret': u'mysecret'}
~~~
Можно комбинировать эти опции вместе с созданием индикатора (сначала он создастся, потом к нему будут применены изменения).

## Партнерский API

Партнерский API доступен только партнерам okerr. Для работы с партнерским API нужно использовать значения api-user и api-pass для вашего партнерского аккаунта.

### Список зарегистрированных пользователей и краткая информация о них
~~~
$ okerrclient --partner-list
[
    {
        "email": "user3@mailinator.com",
        "partner_id": "3",
        "partner_name": "test",
        "username": "user3@mailinator.com"
    },
...
~~~

### Регистрация пользователя (CREATE)
Пользователь будет создан на сервере, к которому выполняется обращение (можно указать его --url. Не путать с --api-url/OKERR_API_URL! api-url - используется когда проект уже создан, и сервер для него известен. Для новых пользователей нужно указывать --url). Возвращается сгенерированный пароль пользователя.

Первый аргумент (partner_id) - внутренний идентификатор пользователя у партнера (например, номер договора или его id в базе или тоже адрес эл. почты).

~~~~
$ okerrclient --url ... --partner-create 1234 delme@example.com
B7LfTdIjTH
~~~~


### Получение информации о пользователе (CHECK)
~~~~
$ okerrclient --partner-check 1234
{
    "email": "delme@example.com",
    "membership": [
        {
            "exp": "20180214",
            "exp_unixtime": 1518615056,
            "group": "Space"
        }
    ],
    "projects": [
        {
            "err": [],
            "name": "delme@example.com",
            "sum": {},
            "textid": "96d4lmssjj"
        }
    ],
    "server": "http://localhost:8000/"
}
~~~~
В проектах показываются только те проекты, для которых включена опция "partner access". После регистрации пользователь может отключить ее.

### Подключение тарифа/услуги пользователю (GRANT)

~~~
$ okerrclient --partner-grant 1234 Diadem
Granted Diadem to user test:1234
~~~

Услуга подключается на 30 дней. Если она уже подключена, то продлевается до 30 дней с сегодняшней даты. Таким образом, несколько повторений
одной команды в один день не меняет ничего на сервере.

Если же нужно подключить заведомо новую услугу (например, второй раз дополнительную услугу), то аналогичным образом используется команда --partner-grant-new:
~~~
$ okerrclient --partner-grant-new 1234 perk:qindicator:1m:10
Granted perk:qindicator:1m:10 to user test:1234
~~~

### Отключение тарифа/услуги (REVOKE)

Так как у пользователя может быть подключено несколько одинаковых услуг, то необходимо указать еще и дату истечения услуги, как она указана
в параметре 'exp' при вызове функции partner-check.
~~~
$ okerrclient --partner-revoke 1234 Diadem 20180213
Revoked Diadem from user test:1234
~~~
