# API

Все функции API доступны через адрес /api/*<имя метода>*. Получение информации - HTTP GET запросами, изменение - HTTP POST. Для указания проекта используется текстовый идентификатор (textid). Для указания индикатора используется его имя.

В примерах ниже мы работаем как пользователь с логином me@example.com, паролем mypass и с проектом с текстовым идентификатором mytextid.

Например (получение списка индикаторов):
~~~
curl --location-trusted -u 'me@example.com:mypass' https://SERVERNAME.okerr.com/api/indicators/mytextid
~~~

## Работа с распределенной системой серверов
Серверная часть okerr представляет состоит из нескольких серверов. Каждый проект находится на одном из них. Иногда (достаточно редко, раз в месяц может быть) проект может переноситься на другой сервер.

Запрос к API должен выполняться именно к серверу, который обслуживает проект на момент запроса. Есть два варианта, как достичь этого:

Вариант 1: Получить информацию о нужном сервере и обратиться к нему:
~~~
curl -u 'me@example.com:mypass' https://cp.okerr.com/api/director/mytextid
https://alpha.okerr.com/

curl -u 'me@example.com:mypass' https://alpha.okerr.com/api/indicators/mytextid
...
~~~
В этом случае, мы сначала узнаем у центрального сервера (по адресу cp.okerr.com ) адрес сервера для нашего проекта, и затем обращаемся к нему. Получать информацию про активный сервер нужно каждый раз перед работой, адрес активного сервера не должен быть вписан (hardcoded) в скрипты. Исходите из "худшего" предположения, что он меняется каждый день.

Вариант 2: Использование механизма HTTP редирект:
~~~
curl -u 'me@example.com:mypass' --location-trusted https://cp.okerr.com/api/indicators/mytextid
~~~
При обращении к серверу, который не обслуживает проект, он обнаружит эту ситуацию, и перекинет на нужный сервер (через HTTP код 302). Ключ --location-trusted необходим для того, чтобы curl высылал аутентификационную информацию и на новый сервер. Этот механизм проще, надежнее, но несколько медленнее - и лучше не использовать его, если нужно выполнить много запросов.

В примерах ниже мы НЕ используем ключ --location-trusted для более коротких примеров (полагаясь, что мы обращаемся сразу к нужному серверу), однако при реальном использовании и обращении к произвольному серверу он необходим.

## Получение информации

### director
~~~
curl -u 'me@example.com:mypass' https://SERVERNAME.okerr.com/api/director/mytextid
https://alpha.okerr.com/
~~~
Дает адрес сервера okerr, который обрабатывает запросы по проекту.


### indicators
Получить список всех индикаторов в проекте с TextID mytextid
~~~
curl  -u me@example.com:mypass https://SERVERNAME.okerr.com/api/indicators/mytextid
~~~
Или, дополнительно отфильтровать по префиксу (первым символам имени):
~~~
curl  -u me@example.com:mypass https://SERVERNAME.okerr.com/api/indicators/mytextid/server1:
~~~




### filter
Получить список индикаторов по фильтру. Фильтруется по свойствам индикатора, которые отражаются в метках (maintenance, disabled, pending, silent, имя политики, имя метода проверки), по пользовательским меткам и по значению аргументов.

Можно фильтровать по нескольким признакам, объединяя их через '/'.

Для "булевых" признаков (например, maintenance или silent), либо просто их указать, чтобы найти индикаторы, где этот признак есть, либо указать с восклицательным знаком (!silent) или минусом (-silent), чтобы найти индикаторы, где признака нет.

Есть ли у нас работающий индикатор, который следит на SSL сертификатом на сервере okerr.com?

~~~
curl  -u me@example.com:mypass https://SERVERNAME.okerr.com/api/filter/mytextid/sslcert/host=okerr.com/-maintenance
~~~



### indicator
Посмотреть все данные по индикатору
~~~
curl  -u me@example.com:mypass https://SERVERNAME.okerr.com/api/indicator/mytextid/indicator_name
~~~



### getarg
посмотреть только 1 аргумент - days
~~~
curl  -u me@example.com:mypass https://SERVERNAME.okerr.com/api/getarg/mytextid/indicator_name/days
~~~


### checkmethods
Посмотреть все доступные методы проверки и их аргументы
~~~
curl -u 'me@example.com:mypass' https://SERVERNAME.okerr.com/api/checkmethods/
~~~


## Изменение информации

### create
Создает индикатор, все параметры по умолчанию:
~~~
curl -u 'me@example.com:mypass' -X POST https://SERVERNAME.okerr.com/api/create/mytextid/delme:new
Created delme:new (OK)
~~~

Обратите внимание - требуется использовать метод POST, но данные в POST не нужны, поэтому мы можем использовать -X POST или --data ''.

### delete
Удаляет индикатор:
~~~
curl -u 'me@example.com:mypass' -X POST https://SERVERNAME.okerr.com/api/delete/mytextid/delme:new
Deleted indicator delme:new from project okrrdm
~~~

Требуется использовать HTTP POST.

### setarg
Изменить аргументы метода проверки. Например, изменить пороговое значение, когда предупреждать о том, что истекает срой действия SSL сертификата:
~~~
curl  -u me@example.com:mypass --data 'days=22' https://SERVERNAME.okerr.com/api/setarg/mytextid/server.com:ssl
Changed: {u'days': u'22'}
~~~

### set
Изменяет параметры индикатора. Параметр, один из:

- policy
- description
- checkmethod
- silent
- disabled
- problem
- retest
- maintenance

Для булевых параметров (silent, disabled, problem, maintenance) их 'включение' выполняется установкой параметра в 1, а выключение - установкой в 0.

При установке retest в любое значение - индикатор будет поставлен на проверку в ближайшее возможное время (аналогично нажатию на "Перепроверить сразу" в веб-интерфейсе).

~~~
curl -u 'me@example.com:mypass' --data 'description=API works!!' https://SERVERNAME.okerr.com/api/set/mytextid/delme:b
Changed delme:b {'description': u'API works!!'}
curl -u 'me@example.com:mypass' --data 'maintenance=1' https://SERVERNAME.okerr.com/api/set/mytextid/delme:b
Changed delme:b {'maintenance': True}
~~~

## Пример

Создание и настройка нескольких индикатор скриптом:

sites.txt:
~~~~
$ cat sites.txt
okerr.com
stackoverflow.com
~~~~

mkssl.sh:
~~~~
#!/bin/sh

FILE=sites.txt
TEXTID=okrrdm
AUTH='me@example.com:mypass'
MAINSRV='https://cp.okerr.com/'
SRV=`curl -s $MAINSRV/api/director/$TEXTID`

echo .. work with server: $SRV

for site in `cat sites.txt`
do
    INAME=ssl:$site
    # delete. just in case
    echo .. delete indicator $INAME    
    curl -u $AUTH -X POST $SRV/api/delete/$TEXTID/$INAME      
    echo .. make indicator $INAME
    curl -u $AUTH -X POST $SRV/api/create/$TEXTID/$INAME  
    echo .. set checkmethod
    curl -u $AUTH --data "checkmethod=sslcert" $SRV/api/set/$TEXTID/$INAME  
    echo .. set arguments
    curl -u $AUTH --data "host=$site&days=20" $SRV/api/setarg/$TEXTID/$INAME  
    echo .. stop maintenance
    curl -u $AUTH --data "maintenance=0" $SRV/api/set/$TEXTID/$INAME  
    echo .. request retest
    curl -u $AUTH --data "retest=1" $SRV/api/set/$TEXTID/$INAME  
    echo
done
~~~~

Результат работы:
~~~~
$ ./mkssl.sh
.. work with server: https://alpha.okerr.com/
.. delete indicator ssl:okerr.com
Project okrrdm has no indicator ssl:okerr.com
.. make indicator ssl:okerr.com
Created ssl:okerr.com (OK)
.. set checkmethod
Changed ssl:okerr.com {'checkmethod': u'sslcert'}
.. set arguments
Changed: {u'host': u'okerr.com', u'days': u'20'}
.. stop maintenance
Changed ssl:okerr.com {'maintenance': False}
.. request retest
Not changed anything

.. delete indicator ssl:stackoverflow.com
Project okrrdm has no indicator ssl:stackoverflow.com
.. make indicator ssl:stackoverflow.com
Created ssl:stackoverflow.com (OK)
.. set checkmethod
Changed ssl:stackoverflow.com {'checkmethod': u'sslcert'}
.. set arguments
Changed: {u'host': u'stackoverflow.com', u'days': u'20'}
.. stop maintenance
Changed ssl:stackoverflow.com {'maintenance': False}
.. request retest
Not changed anything
~~~~
