# Решение проблем

## SNIMissingWarning и InsecurePlatformWarning
~~~~
/usr/local/lib/python2.7/dist-packages/requests/packages/urllib3/util/ssl_.py:334: SNIMissingWarning: An HTTPS request has been made, but the SNI (Subject Name Indication) extension to TLS is not available on this platform. This may cause the server to present an incorrect TLS certificate, which can cause validation failures. You can upgrade to a newer version of Python to solve this. For more information, see https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
~~~~
и
~~~~
/usr/local/lib/python2.7/dist-packages/requests/packages/urllib3/util/ssl_.py:132: InsecurePlatformWarning: A true SSLContext object is not available. This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail. You can upgrade to a newer version of Python to solve this. For more information, see https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
~~~~

Проблема возникает из-за использования устаревших питоновский библиотек в области криптографии. Необходимо их обновить. Однако, для обновления может потребоваться более современная версия pip, поэтому правильная схема решения: сначала обновить pip, и затем установить необходимые библиотеки:

~~~~
pip install --upgrade pip
pip install 'requests[security]'
~~~~
