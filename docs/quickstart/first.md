## Первый индикатор

### Задача
Для начала, будем использовать okerr, чтобы автоматически отслеживать изменения на какой-нибудь странице. Например, пусть okerr будет следить за страницей [http://bash.im/rss/](http://bash.im/rss/) - и известит нас, когда появится что-то свеженькое. 

### Решение

Внизу главной страницы контрольной панели, расположена форма добавления индикатора.
Указываем имя индикатору, (например, "test:first" - очень хорошее имя), жмем на зеленый плюсик. Он появится в списке. Настроим его - переходим на страницу индикатора нажав на имя индикатора. Созданный таким образом индикатор изначально будет иметь метод проверки "**heartbeat**" и будет иметь статус "**maintenance**" (находится в режиме настройки).

Изменим метод проверки на "**HTTP SHA1 hash dynamic**" и нажмем "**Изменить**" в разделе "**Команды**". Ниже, в разделе "**Аргументы**" появятся два аргумента (специфичные для метода HTTP SHA1 hash dynamic) **url** и **hash**. Укажем url - [http://bash.im/rss/](http://bash.im/rss/) и нажмем "Сохранить аргументы" (поле hash заполнять не надо).

В общем-то все сделано. Теперь проверим, как это работает. Нажмем кнопку "**Перепроверить сразу**". В поля "Ожидается" и "Запланировано" запишется текущее системное время (в часовом поясе UTC), а индикатору будет выставлена метка "**pending**" (ожидает обработки). Через несколько секунд, система исполнит проверку - поле "Обновлено" выставится во время, когда система его обработала, метка pending исчезнет, а в поле hash будет записан sha1 хеш странички.

В дальнейшем, когда мы будем говорить о следующей проверке - можно не ждать, когда okerr выполнит ее по расписанию, а просто нажать кнопку "**Перепроверить сразу**" и проверка будет выполнена в течение следующих нескольких секунд.

Дальше, при каждой следующей проверке, okerr будет сравнивать хеш странички с сохраненным значением и если оно изменилось - запишет новое значение в поле hash.

Теперь - было бы неплохо, чтобы okerr сам уведомлял вас на почту об изменениях. Это делается автоматически, для этого нужно отключить режим настройки на индикаторе. (О том, что действует режим настройки говорит сообщение над информацией об индикаторе, время в поле "Режим настройки" и метка "**maintenance**" ). Отключим режим настройки нажав кнопку "**Выйти из режима настройки**". Все, теперь, как только система обнаружит, что страничка изменилась - она вышлет вам оповещение. Время, когда okerr выполнит проверку - указано в поле "Ожидается", и зависит от выбранной политики индикатора. По-умолчанию - проверка назначается на время через час после предыдущей выполненной проверки.

Отныне все просто - если на баше появится новая интересная цитата - вам придет письмо. Если же писем от окерра нет - то и на баш можно не ходить - там все равно ничего нового.


