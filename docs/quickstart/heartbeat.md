
## Пип - пип - пип - пииии...

> Глупая система мониторинга постоянно спрашивает сервер: "Ты не умер? А сейчас? А сейчас не умер?", наполняя жизнь сервера суетой и ненужными мыслями. Мониторинг, который постиг истинный дзен - молча кладет руку на сердце, и ждет, когда оно перестанет биться.

Выше мы рассмотрели несколько внешних типов проверки, но невероятная мощь okerr состоит в открытом интерфейсе, благодаря которому на платформе okerr можно реализовать любую проверку, в том числе и внутреннюю.

### Задача
Требуется наблюдать нечто специфичное на сервере, то что не поддерживается окерром "из коробки". Например, будем следить за тем, что процесс apache2 запущен.


### Решение
Задача разбивается на две подзадачи. Первая - определение состояния скриптом, вторая - передача этого состояния на сервер.

Если мы будем решать первую подзадачу через шелл-скрипт, он у нас будет выглядеть примерно так:

    
    #!/bin/sh

    PROCNAME='apache2'

    if pidof $PROCNAME > /dev/null
    then
        echo OK
    else
        echo ERR
    fi
 
Довольно просто, не так ли?

Теперь разберемся, как можно передавать это на сервер. Есть несколько путей, рассмотрим самый простой - через клиент okerrclient.

Сначала нужно поставить клиент:
    
    sudo pip install okerrclient

У каждого проекта в okerr есть свой уникальный Text ID, нам нужно знать его. Можно просто навести мышку на название проекта (по умолчанию - первый проект называется как ваш адрес электронной почты) в строчке с названием проекта и иконками. Появится tooltip-подсказка с TextID. 

![Imgur](http://i.imgur.com/O4Hb8n8.png)

Или же, перейти в раздел "Проекты" и там для каждого проекта будут перечислены его текстовые идентификаторы. 
![Imgur](http://i.imgur.com/ZiuiaSA.png)

Cоздать и управлять индикаторами в этом проекте, можно очень простой командой, вроде:

    okerrclient -i <TextID>  -s 'NAME test:apache2' 'OK'

Только вместо <TextID> укажите ваш текстовый идентификатор проекта. После этой команды обновите главную страничку проекта в браузере, и вы должны увидеть, что у вас появился индикатор test:apache2, и у него статус OK (зеленая лампочка).

Чтобы установить статус ошибки, можно воспользоваться аналогичной командой:

    okerrclient -i <TextID>  -s "NAME test:apache2" ERR

(да, как видите, есть определенная свобода в использовании кавычек)

Что делает эта команда? Опция **-i** задает TextID, чтобы okerr знал, для какого из проектов к нему пришло обновление статуса индикатора. Опция **-s** - наиболее важная - следом за ней идет последовательность инструкций, которые выполнит внутренний интерпретатор в клиенте. Если инструкция состоит из более чем одного слова, она должна быть заключена в кавычки (для удобства - лучше все, даже однословные инструкции заключать в кавычки). В данном случае у нас всего две инструкции - первая (NAME) устанавливает имя индикатора (test:apache2). А вторая (команда OK или ERR) - устанавливает соответствующий статус.

В итоге, скрипт выглядит так:

    #!/bin/sh

    PROCNAME="apache2"
    INAME="test:$PROCNAME"
    TEXTID="qweqwe"

    if pidof $PROCNAME > /dev/null
    then
        okerrclient -i $TEXTID -s "NAME $INAME" "OK"
    else
        okerrclient -i $TEXTID -s "NAME $INAME" "ERR"
    fi

Можете использовать этот скрипт как шаблон для наблюдения других параметров. Запускайте его из крона, например, раз в час (период времени в политике по умолчанию) - и если вдруг что - вы будете в курсе!

Вас можно поздравить - теперь вы можете посылать любые сигналы с любых серверов, централизованно отслеживать любые процессы внутри них. Мы рассмотрели простейший случай, но дальше будет интереснее!

Обратите внимание на приятный бонус. Что будет если вдруг на сервере что-то слетит? Может быть вы или другой админ по ошибке удалит этот скрипт из крона. Или почему-то вдруг поломается okerrclient. Или в результате нашествия зомби весь дата-центр окажется обесточен а его сотрудники съедены... Это неприятные вещи и вы должны быть о них в курсе. И вы будете! В этих случаях на сервер окерр перестанут поступать сигналы OK про этот индикатор он автоматически перейдет в статус ошибки (ERR) и вышлет вам сообщение. CNN еще будет готовить срочный сюжет, президент будет мчаться на секретное совещание с генералами, а вы уже будете знать, что кое-что пошло не так. Ваш сервер, подключенный к okerr уже готов к зомби-апокалипсису!


### Дополнение

В целях безопасности есть ряд ограничений, которые задаются *политикой*. (Для каждого индикатора используются значения из той политики, которая с ним связана. Для новых индикаторов - всегда используется политика Default)

В политике может быть прописан *Secret* - секретное значение, которое необходимо передавать для изменения статуса индикаторов. (Если поле пустое, то секрет не требуется). Для указания секрета в okerrclient используется опция -S.

Кроме этого, в политике указываются IP адреса (и подсети), с которых допускается принимать обновления статусов. По-умолчанию в списке разрешенных все адреса (0.0.0.0/0). 

Так же есть три важных флага: *Accept updates over HTTP*, *Accept updates over SMTP* и *Autocreate*.

Первые два разрешают прием обновлений статусов через соответствующие каналы. Например, если убрать галочку с Accept updates over HTTP, то наш обновления через okerrclient не будут работать.

Если флаг Autocreate установлен, тогда при попытке установить статус индикатору, которого не существет - он автоматически создастся.

